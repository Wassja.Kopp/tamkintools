from tamkin import *
from molmod import * 
import numpy
# functions
def enthalpy_NASA(NASA_T, NASA_a):        # returns (h-h0)/RT from NASA polynomial
	"Computes enthalpy from NASA coefficients"
        return NASA_a[0] + NASA_T*(NASA_a[1]/2. + NASA_T*(NASA_a[2]/3. + NASA_T*(NASA_a[3]/4. + NASA_T*NASA_a[4]/5.))) + NASA_a[5]/NASA_T

def entropy_NASA(NASA_T, NASA_a):         # returns s/R from NASA polynomial
        "Computes entropy from NASA coefficients"
        return NASA_a[0]*numpy.log(NASA_T) + NASA_a[6] + NASA_T*(NASA_a[1] + NASA_T*(NASA_a[2]/2. + NASA_T*(NASA_a[3]/3. + NASA_T*NASA_a[4]/4.)))
    
def heatcapacity_NASA(NASA_T, NASA_a):        # returns (cp)/R from NASA polynomial
	"Computes heat capacity from NASA coefficients"
	return NASA_a[0] + NASA_T*(NASA_a[1] + NASA_T*(NASA_a[2] + NASA_T*(NASA_a[3] + NASA_T*NASA_a[4])))

def nasafit(pf, hf, T1, T2, TG=1000.0, T0=298.15, _R=8.315):
  T       = numpy.append(T1,T2)
  h0 = pf.internal_heat(T0)/kjmol # internal heat means enthalpy since calc is NpT
                                  # the enthalpy has to be passed unscaled (not scaled by 1/RT) in kJ/mol
  h0-=hf
  h0 = h0*kjmol # convert to hartree / particle

  h1      = []                          # y1, will contain  h/RT
  h2      = []                          # y2

  s       = []

  X       = numpy.zeros((len(T),14),float) # Enthalpy expressions
  
  for i in xrange(len(T1)):
      h1.append((pf.internal_heat(T1[i]) - h0)/(kjmol*T1[i]*_R/1000.0)) # fit to H/RT
      s.append(pf.entropy(T1[i])/(joule/(mol*kelvin)))
      #print T1[i],"   ",h1[i],"\t",h1[i]*T1[i]*_R/1000.0
      
      X[i,0] = 1.0		      # a1,I
      X[i,1] = T1[i]/2.0              # a2,I
      X[i,2] = T1[i]**2/3.0           # a3,I
      X[i,3] = T1[i]**3/4.0           # a4,I
      X[i,4] = T1[i]**4/5.0           # a5,I
      X[i,5] = 1.0/T1[i]              # a6,I
      X[i,6] = 0                    # a7,I
      
      X[i,7] = 0                    # a1,II
      X[i,8] = 0                    # a2,II
      X[i,9] = 0                    # a3,II
      X[i,10] = 0                   # a4,II
      X[i,11] = 0                   # a5,II
      X[i,12] = 0                   # a6,II
      X[i,13] = 0                   # a7,II
      
  for i in xrange(len(T2)):
      h2.append((pf.internal_heat(T2[i]) - h0)/(kjmol*T2[i]*_R/1000.0)) # fit to H/RT
      #print T2[i],"   ",h2[i]
    
  for i in xrange(len(T1),len(T2)+len(T1)):
      j = i - len(T1)
      s.append(pf.entropy(T2[j])/(joule/(mol*kelvin)))
      X[i,0] = 0                    # a1,I
      X[i,1] = 0                    # a2,I
      X[i,2] = 0                    # a3,I
      X[i,3] = 0                    # a4,I
      X[i,4] = 0                    # a5,I
      X[i,5] = 0                    # a6,I
      X[i,6] = 0                    # a7,I
      
      X[i,7] = 1.0                    # a1,II
      X[i,8] = T2[j]/2.0              # a2,II
      X[i,9] = T2[j]**2/3.0           # a3,II
      X[i,10] = T2[j]**3/4.0          # a4,II
      X[i,11] = T2[j]**4/5.0          # a5,II
      X[i,12] = 1.0/T2[j]           # a6,II
      X[i,13] = 0                   # a7,II
      
  y = numpy.array(h1 + h2)
  R = numpy.zeros((6,14),float)        # Restrictions

  R = [[ 1.0, TG/2.0, TG**2/3.0, TG**3/4., TG**4/5., 1./TG, 0, -1, -TG/2., -TG**2/3., -TG**3/4., -TG**4/5., -1./TG, 0 ], # HI = HII
       [ 1.0, TG  , TG**2  , TG**3  , TG**4  , 0   , 0, -1, -TG  , -TG**2  , -TG**3  , -TG**4  ,  0   , 0 ], # cpI = cpII
       [ 0, 1.0   , 2.0*TG   , 3.*TG**2, 4.*TG**3, 0   , 0,  0, -1   , -2.*TG   , -3.*TG**2, -4.*TG**3,  0   , 0 ], # d/dT cpI = d/dT cpII
       [ 1., T0/2., T0**2/3., T0**3/4., T0**4/5., 1./T0, 0,  0,  0   ,  0      ,  0      ,  0      ,  0   , 0 ], # Hf(T0)
       [ numpy.log(T0), T0, T0**2/2., T0**3/3., T0**4/4., 0 , 1., 0,0,0,0,0,0,0],                              # S(T0) = Sf
       [ numpy.log(TG), TG, TG**2/2., TG**3/3., TG**4/4., 0 , 1., -numpy.log(TG), -TG, -TG**2/2., -TG**3/3., -TG**4/4., 0 , -1.]] # SI = SII

  r = numpy.zeros(6,float)
  r = [0, 0, 0, hf/(T0*_R/1000.), pf.entropy(T0)/(_R*joule/(mol*kelvin)), 0] # _R in units of J / mol kelvin
    
  M = numpy.vstack(( numpy.hstack(( numpy.dot(numpy.transpose(X),X),numpy.transpose(R))),
                   numpy.hstack(( R,numpy.zeros((6,6),float) )) ))

  v = numpy.append( numpy.dot(numpy.transpose(X),y), numpy.array(r) )
  coeffs    = numpy.zeros(14,float)
  lagr_mult = numpy.zeros( 6,float)  
  solve = numpy.linalg.solve(M,v)
  coeffs = solve[:len(coeffs)]
  lagr_multi = solve[len(coeffs):]
  
  HC = []
  for i in xrange(len(T)):
     HC.append(pf.heat_capacity(T[i])/(kjmol*kelvin)*1000) # J/molK
    # print T[i],"   ",HC[i]
  
  #  0 , 1     ,  2       ,  3            , 4          , 5              , 6                                  , 7         , 8
  #  T , h_NASA, h_comp/RT, h_NASA*RT/1000, h_comp/1000, s_comp in joule, s_NASA*R(as passed to the function), c_p_NASA*R, cp_pf in kJ/mol/kelvin
  enthalpy = numpy.zeros((len(T),9),float)
  enthalpy[:len(T1),0] = T1
  enthalpy[len(T1):,0] = T2
  enthalpy[:len(T1),1] = enthalpy_NASA(T1, coeffs[:7])
  enthalpy[len(T1):,1] = enthalpy_NASA(T2, coeffs[7:])
  enthalpy[:,3] = enthalpy[:,1]*enthalpy[:,0]*_R/1000.
  enthalpy[:len(T1),2] = h1
  enthalpy[len(T1):,2] = h2
  enthalpy[:,4] = enthalpy[:,2]*enthalpy[:,0]*_R/1000.
  enthalpy[:,5] = s
  enthalpy[:len(T1),6] = entropy_NASA(T1,coeffs[:7])*_R
  enthalpy[len(T1):,6] = entropy_NASA(T2,coeffs[7:])*_R
  enthalpy[:len(T1),7] = heatcapacity_NASA(T1, coeffs[:7])*_R
  enthalpy[len(T1):,7] = heatcapacity_NASA(T2, coeffs[7:])*_R
  enthalpy[:,8] = HC
  
  errorfit  = numpy.zeros(2, float)
  errorfit[0]  = numpy.sum((enthalpy[:,1]-enthalpy[:,2])**2)
  errorfit[1]  = numpy.sum((enthalpy[:,6]-enthalpy[:,5])**2)
  
  return enthalpy, coeffs, lagr_multi, errorfit
