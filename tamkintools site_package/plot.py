from tamkin import *
from tamkintools import *
import numpy as np
from matplotlib import pyplot as plt
from molmod import *

def plot_levels_all(self, prefix, temp, do_levels=True, do_data=True,erased_points = [],added_points=[]):
        """Plots the potential with the energy levels

           Arguments:
            | ``prefix`` -- A filename prefix for the png files.
            | ``temp`` -- A temperature that is used to indicate the statistical
                          weight of each level in the plots

           Optional argument:
            | ``do_levels`` -- When True, the energy levels are plotted.
                               [default=True]
            | ``do_data`` -- When True, the data points are plotted.
                             [default=True]

           One image will be generated:
            | ``${prefix}.png`` -- The potential and the energy levels.
        """
        import matplotlib.pyplot as pt
        pt.clf()
        if do_data:
            # plot the original potential data 
            if self.rot_scan.potential is not None:
                angles, energies = self.potential
                print('the angles are')
                print(angles)
                print('length of angle array is: ')
                print(len(angles))
                pt.plot(angles/deg, energies/kjmol, "gx", mew=2)
        if len(added_points) > 0:
            added_angles,added_energies = added_points
            print('added angles are:')
            print(added_angles)
            print('added energies are: ')
            print(added_energies)
            pt.plot(np.asarray(added_angles)/deg, np.asarray(added_energies)/kjmol, 'rx',mew=2,color='red') ## New line for added points 
         
        if len(erased_points) > 0:
            erased_angles,erased_energies = erased_points[1]
            pt.plot(np.asarray(erased_angles)/deg,np.asarray(erased_energies)/kjmol,'go',mew =2) ## New line for erased points
                   
            pt.axvline(self.nma_angle/deg, color="silver")
        # plot the fitted potential
        if self.hb is not None:
            step = 0.001
            x = np.arange(0.0, 2*np.pi*(1+0.5*step), 2*np.pi*step)
            v = self.hb.eval_fn(x, self.v_coeffs)
            pt.plot(x/deg, v/kjmol, "k-", linewidth=2)
        if do_levels:
            # plot the energy levels
            eks = self.energy_levels/(temp*boltzmann)
            bfs = np.exp(-eks)
            bfs /= bfs.sum()
            for i in range(self.num_levels):
                e = (self.energy_levels[i])/kjmol
                pt.axhline(e, color="b", linewidth=0.5)
                pt.axhline(e, xmax=bfs[i], color="b", linewidth=2)
        if self.rot_scan.potential is not None:
            angles, energies = self.potential
            pt.ylim(
                energies.min()/kjmol,
                1.5*energies.max()/kjmol
            )
            fit_energies = self.hb.eval_fn(angles, self.v_coeffs)
            rmsd = ((fit_energies - energies)**2).mean()**0.5
            rms = (energies**2).mean()**0.5
            rrmsd = rmsd/rms
            title = 'RMSD [kJ/mol] = %.1f    RRMSD [%%] = %.0f    dofmax = %i   rotsym = %i' % (
                rmsd, rrmsd*100, self.dofmax, self.rotsym
            )
            if self.even:
                title += '   even'
            pt.title(title)
        pt.xlim(0, 360)
        pt.ylabel("Energy [kJ/mol]")
        pt.xlabel("Dihedral angle [deg]")
        pt.savefig(prefix)

