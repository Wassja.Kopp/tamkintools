import numpy
import re

####################
# Checking routine #
####################

def checkCC(fname, txt):
        F = open(fname, 'r')
        f = F.read()
        F.close()

        print ""
        print "Checking routine:"
        print "File name:", F.name

        erg = re.search(txt,f)
        if erg == None:
                print "CCSD(T) not found"
        else:
                energy = float(erg.group(1))*10**float(erg.group(2))
                print "CCSD(T) =", energy

        return energy
        F.close()

def checkHF(fname):
        F = open(fname, 'r')
        f = F.read()
        F.close()

        print ""
        print "Checking routine:"
        print "File name:", F.name

        erg = re.search("SCF Done:\s+(E\(\w+\))\s*\=\s*(\-?\d+\.\d+)",f)
        if erg == None:
                print "No 'SCF Done' found"
        else:
                energy = float(erg.group(2))
                print str(erg.group(1)),"=", energy

        return energy
        F.close()

def checkMP2(fname):
        F = open(fname, 'r')
        f = F.read()
        F.close()

        print ""
        print "Checking routine:"
        print "File name:", F.name

        erg = re.search("E2 =\s*\-?\d+\.\d+D[+-]\d+\s+(EUMP2)\s*\=\s*(\-?\d+\.\d+)D([+-]\d+)",f)
        if erg == None:
                print "No 'E2' found"
        else:
                energy = float(erg.group(2))*10**(float(erg.group(3)))
                print str(erg.group(1)),"=", energy

        return energy
        F.close()

def checkMP4(fname):
        F = open(fname, 'r')
        f = F.read()
        F.close()

        print ""
        print "Checking routine:"
        print "File name:", F.name

        erg = re.search("E4\(SDQ\)=\s*\-?\d+\.\d+D[+-]\d+\s+(\w+MP4)\(SDQ\)\s*\=\s*(\-?\d+\.\d+)D([+-]\d+)",f)
        if erg == None:
                print "No 'E4(SDQ)' found"
        else:
                energy = float(erg.group(2))*10**(float(erg.group(3)))
                print str(erg.group(1)),"=", energy

        return energy
        F.close()

def checkT1(fname):
        F = open(fname, 'r')
        f = F.read()
        F.close()

        print ""
        print "Checking routine:"
        print "File name:", F.name

        erg = re.search("T1 Diagnostic\s*\=\s*(\d+\.\d+)",f)
        if erg == None:
                print "No 'T1 Diagnostic' found"
        else:
                energy = float(erg.group(1))
                print "T1 Diagnostic =", energy

        return energy
        F.close()

#####################
# Checking elements #
#####################

def elements(fname, N):
        F = open(fname, 'r')
        lines = F.read().splitlines()
        F.close()

        print ""
        print "Checking elements:"
        print "File name:", F.name

        nC = 0
        nH = 0
        nO = 0

        l0 = lines.index(" Title Card Required")

        for i in xrange(l0+4,l0+4+int(N)):
                erg = re.search(r"([CHO]+)(\s+)(\-?)(\d+)\.(\d*)(\s+)(\-?)(\d+)\.(\d*)(\s+)(\-?)(\d+)\.(\d*)", str(lines[i]))
                if erg.group(1) == "C":
                        nC += 1
                elif erg.group(1) == "H":
                        nH += 1
                elif erg.group(1) == "O":
                        nO += 1
                else:
                        print "There is an element that is not C, H or O!"

        print "C = ", nC
        print "H = ", nH
        print "O = ", nO

        return nC, nH, nO
        F.close()

#####################################
# Single point energies calculation #
#####################################

def specalc(E2,E3):
        a = (216.*(E2-E3))/(19)
        Einf2 = E2 - a/8.
        Einf3 = E3 - a/27.
        Einf = numpy.mean([Einf2,Einf3])
        print ""
        print "Single point energies calculation with extrapolation scheme"
        print "a =", a
        print "Einf =", Einf
        return a, Einf

###################
# Number of atoms #
###################

def atomnum(fname):
        F = open(fname, 'r')
        lines = F.read().splitlines()
        F.close()

        print ""
        print "Checking number of atoms:"
        print "File name:", F.name

        erg = re.search(r"(\w+)(\s+)(\w+)(\s+)(\d+)", str(lines[2]))
        N = erg.group(5)

        print "Number of atoms:", N

        return N
        F.close()

