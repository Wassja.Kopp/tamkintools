from tamkin import *
from tamkin.data import RotScan
import numpy as np
from molmod import deg, kjmol

'''
This script connects two scans of the same molecule. 
The function is called as follows:
Scan1 =load_rotscan_g03log("scan1.log")
Scan2 =load_rotscan_g03log("scan2.log")
Combined_Scan=combine_Scan(Scan1, Scan2)
'''

def combine_Scan(Scan1:RotScan, Scan2:RotScan):

    #check if both Scan-objects involve the same index of atoms
    if not np.all(Scan1.dihedral) == np.all(Scan2.dihedral):
        raise ValueError("The two RotScan objects do not share the same index of atoms that define the dihedral angle.")
    
    #concatenate the potentials
    combined_potential=np.concatenate((Scan1.potential, Scan2.potential), axis=1)

    #merge all attributes with class tamkin.data.RotScan(dihedral, molecule=None, top_indexes=None, potential=None)
    result = RotScan(dihedral=Scan1.dihedral, top_indexes=Scan1.top_indexes, potential=combined_potential)  # creates a new scan-- ROTSCAN is well documented in TAMKIN
    return result

# Not sure if it works 
#TODO check it if it works
# Combine 2,5DMF using this function - done
def potential_reference(Scan: RotScan,nma_angle):
    """
       Determines potential energy from a rotscan object
    """
    if Scan.potential is None:
        return None
    else:
        a = 2*np.pi
        angles, energies = Scan.potential.copy()
        # apply periodic boundary conditions
        angles -= np.floor(angles/a)*a
        # set reference energy, which is take to be the energy of the geometry
        # with a dihedral angle closest to the dihedral angle of the reference
        # geometry in the nma object. We can not take the nma energy because
        # the rotational energy barriers may be computed at another level
        # that the nma energy.
        deltas = angles - nma_angle
        # apply periodic boundary conditions
        #deltas -= np.floor(deltas/a)*a
        deltas -= (abs(np.floor(deltas/a))-0.5)*a
        deltas = abs(deltas)
        return energies[deltas.argmin()]

def erase_point_scan(Scan: RotScan, index: int):
    # Erases the specified point in the scan - so that it does not get plotted - returns the scan without the point
    tmpangles, tmpenergies = Scan.potential
    newangles = []
    newenergies = []
    
    counter = 0
    for angle in tmpangles:
        if counter != index:
            newangles.append(tmpangles[counter])
            newenergies.append(tmpenergies[counter]) 
        counter += 1
    newpotential = [newangles,newenergies]
    #erased_result = RotScan(dihedral=Scan.dihedral, top_indexes=Scan.top_indexes, potential = erased_potential)
    result = RotScan(dihedral=Scan.dihedral, top_indexes=Scan.top_indexes, potential = newpotential)
    return result

def erase_point(Scan:RotScan, index:int):
    "returns the erased point - used in optimized-rotor.py in plot_levels_all"
    tmpangles, tmpenergies = Scan.potential
    erased_angle = []
    erased_energy = []
    counter = 0
    for angle in tmpangles:
        if counter == index:
            erased_angle.append(tmpangles[counter])
            erased_energy.append(tmpenergies[counter])
        counter +=1
    erased_potential = [erased_angle, erased_energy]
    print('erased_potential in combine_scans are: ' + str(erased_potential))
    return erased_potential

def add_point(Scan:RotScan, angle:float, energy:float):
    # adds a point to the scan -- points are specified by the user (angle and an energy) -- angle is in radians and energy in Hartree/particle
    tmpangles, tmpenergies = Scan.potential
   # newangles = []
    #newenergies = []
    #newangles.append(tmpangles)
    #newenergies.append(tmpenergies)
    #print('energies to be added are: ' + str(energy))
    #print('ref_energy is: ' + str(ref_energy))
    #print('ref_energy with energy is: ' + str((energy)/kjmol)
    newpotential=np.concatenate((Scan.potential,[[angle],[energy]]),axis =1)
    #newangle=np.co]ncatenate((tmpenergy,energy),axis =1
    #newpotential = [newangles,newenergies],
    result = RotScan(dihedral=Scan.dihedral, top_indexes=Scan.top_indexes, potential = newpotential)
    return result

	
