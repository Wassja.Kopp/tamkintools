from tamkin import *
import numpy as np
import cmath

# This script integrates the wavefunction to calculate the residence probability
# --> int psi*psi dtheta  [theta1, theta2]
#
###################################################################################
'''
WaveFunction von Tamkin 
(damit sind „coeffs“ gemeint? Ja, zusammen mit den implizit benutzten sinussen und cosinussen)-> GFF Objekt
--> Was ist die Verknüpfung „coeffs“ und Wellenfunktion? Tamkin erwähnt das nicht explizit in ihrer Description. Ist genauso wie bei der Fourierreihe. Der erste Koeffizient ist der konstante Teil, also *1, dann sinus 1. Ordnung, dann cosinus 1. Ordnung, und so weiter
--> Gleichung für die Transformation ist: a*exp(1j*theta)+b*exp(-1j*theta)=(a-b)*1j*sin(theta)+(a+b)*cos(theta)
      In obiger Gl. Bewegen wir uns von rechts nach links (ist mit theta hier und weiter oben beides mal dasselbe gemeint? ich gehe davon aus) Ja, das theta ist das gleiche. Was du mit rechts nach links meinst, verstehe ich nicht.
Diese Funktion steht schon so gut wie – ja, du hattest vor Monaten mal die Aufgabe gehabt, Transformationsfunktionen zu machen Tamkin to GFF und GFF to Tamkin.
'''
num_levels=50
temperature=1000.0
# Richtigen Datentyp erhalten
butane =load_molecule_g03fchk("butane_blyp.fchk")
but_scan =load_rotscan_g03log("butane_blyp_scan.log")
nma1 =NMA(butane, ConstrainExt(gradient_threshold=0.1))
rotor1 =Rotor(but_scan, butane, rotsym=1, even=True, dofmax=7, num_levels=num_levels)
pf1 =PartFun(nma1,[ExtTrans(), ExtRot(symmetry_number=1), rotor1])
#print(type(rotor1.v_coeffs))

# determination of mass: (wie machen wir das mit opt_rotor?)
if rotor1.large_fixed:
    moment = rotor1.moment
else:
    moment = rotor1.reduced_moment
mass = moment

energies, wfns = rotor1.hb.solve(mass, rotor1.v_coeffs, evecs=True)
#print(energies[0])
#print(wfns)
'''
wfns[i]format:
[const.    sin1    cos1    sin2  cos2    sin3 usw

gff format:
[const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.

transformation:
a*sin(theta)+b*cos(theta)=(a/(2j)+b/2)*exp(1j*theta)+(-a/(2j)+b/2)*exp(-1j*theta)
'''
wfns_t = np.transpose(wfns)
gff_list = []
for i in range(len(wfns_t[:,0])):
    gff = np.array([]) 
    # second index of wfns_t is terms of fourier series
    gff = np.append(gff,wfns_t[i][0]) #constant term
    for k in range(1, len(wfns_t[i])):
        if k % 2 != 0:
            a = wfns_t[i][k]   #coeff cos
            b = wfns_t[i][k+1] #coeff sin
            c = (b/(2j))+(a/2) #1/2*(a-ib)
            d = (-b/(2j))+(a/2)#1/2*(a+ib)
            gff = np.append(gff,c)
            gff = np.append(gff,d)
    gff_list.append(gff)
coll_gffs = np.vstack(gff_list)
#print("coll_gffs ",coll_gffs)
#print(wfns[0])

###################################################################################
'''
Multiplizieren 
(also psi*psi in der erstellten gff Schreibweise durchführen?) Genau, unser GFF Modul überschreibt für GFF Objekte die Operatoren plus und mal. Man kann einfach psi*psi schreiben.

gff format:
[const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.
'''
#multi=psi*psi
#print(multi)


###################################################################################
'''
Integrieren 
(Dann das erhaltene Polynom (bzw. dass gff Object) mit den ganzen e-Funktionstermen (bzw. das GFF objekt) integrieren?) Es gibt auch schon eine Funktion, die über den gesamten Raum integriert, ich meine sie heißt .integrate(). Da müsste man vielleicht noch die Dimension angeben (0 oder 1) und dann das (konstante) Resultat an einem beliebigen Punkt auswerten.
--> Das Integrationsschema müsste recht einfach sein bei e-Funktionen. Genau, deshalb haben wir es auch schon implementiert. Du brauchst da nichts zu programmieren, das ist schon fertig. Einfach aufrufen.

gff format:
[const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.

stacked gff format:
[[const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.   |
 [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.   |   |
 [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.   |   | index k
 [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.   |   V
 [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.   |

----------------------------------------------------------------------
                       ------>
                       index h

Bsp. mit numlevels=3
coll_gff:
[[ 0.50555262+0.j          0.11622152+0.19690006j  0.11622152-0.19690006j
   0.12561233-0.09417304j  0.12561233+0.09417304j -0.17980430-0.27721915j
  -0.17980430+0.27721915j]
 [-0.52794541+0.j         -0.01482680+0.15999072j -0.01482680-0.15999072j
   0.17456886+0.21159085j  0.17456886-0.21159085j  0.16134459-0.23070434j
   0.16134459+0.23070434j]
 [-0.04727210+0.j         -0.27017904-0.23308523j -0.27017904+0.23308523j
   0.24333308+0.05244715j  0.24333308-0.05244715j -0.23956772-0.05255017j
  -0.23956772+0.05255017j]
 [ 0.49546747+0.j         -0.10800110-0.19848202j -0.10800110+0.19848202j
  -0.12063583+0.12882094j -0.12063583-0.12882094j  0.24370916-0.21685629j
   0.24370916+0.21685629j]
 [-0.01540389+0.j          0.24381658-0.21953892j  0.24381658+0.21953892j
   0.27767839-0.18130045j  0.27767839+0.18130045j  0.17948954+0.01025575j
   0.17948954-0.01025575j]
 [-0.45650817+0.j          0.10839058-0.17504244j  0.10839058+0.17504244j
  -0.22983436-0.13326758j -0.22983436+0.13326758j -0.13543453-0.2580409j
  -0.13543453+0.2580409j ]
 [-0.09659470+0.j         -0.28357844+0.11433419j -0.28357844-0.11433419j
   0.00735804-0.35550684j  0.00735804+0.35550684j  0.15586045-0.05870768j
   0.15586045+0.05870768j]]
'''
'''
#GFF format:
contains the coefficients for these terms in that order:
[const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.

#coll_gff format:
for each eigenstate i:
i=1             [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.
i=2             [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.
i=3             [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.
...             ...
i=len(energies) [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.

#conj_coll_gffs format:
but this time all the coefficients are complex conjugated
for each eigenstate i:
i=1             [const. exp(-1j*theta) exp(1j*theta) exp(-2j*theta) exp(2j*theta) usw.
i=2             [const. exp(-1j*theta) exp(1j*theta) exp(-2j*theta) exp(2j*theta) usw.
i=3             [const. exp(-1j*theta) exp(1j*theta) exp(-2j*theta) exp(2j*theta) usw.
...             ...
i=len(energies) [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.

#mult_coll_gffs format:
here conj_coll_gffs[i] and coll_gffs[i] have been multiplied and the result was put back into the right order of the GFF
for each eigenstate i:
i=1             [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.
i=2             [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.
i=3             [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.
...             ...
i=len(energies) [const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.

#result_coll_gffs format:
here the results of the integration of mult_coll_gffs[i] over the input theta value are saved over each eigenstate i:
for each eigenstate i:
i=1             integrated mult_coll_gffs[1]
i=2             integrated mult_coll_gffs[2]
i=3             integrated mult_coll_gffs[3]
...             ...
i=len(energies) integrated mult_coll_gffs[len(energies)]
'''

# we want to integrate psi(compl.conj.)*psi

def gff_to_coeffs(gffs):
    coeffs_list = []
    for i in range(len(gffs[:,0])):
        coeffs = np.array([])
        coeffs = np.append(coeffs,gffs[i][0].real) #constant term
        for k in range(1, len(gffs[i])):
            if k % 2 != 0:            #index 1,3,5....
                a = gffs[i][k]        #positive exponent
                b = gffs[i][k+1]      #negative exponent
                c = (a+b)             #coeff for cos
                d = (a-b)*1j          #coeff for sin
                coeffs = np.append(coeffs,c.real)
                coeffs = np.append(coeffs,d.real)
        coeffs_list.append(coeffs)
    coll_coeffs = np.vstack(coeffs_list)
    return coll_coeffs    

#conjugation:
# for this we create with "conj_coll_gffs" a new gff that is the conjugate version of coll_gffs
conj_coll_gffs = np.conj(coll_gffs) #in addition all positive and negative orders are switched, keep that in mind
#print(conj_coll_gffs)

conj_coll_coeffs = gff_to_coeffs(conj_coll_gffs)

#print("wfns ", wfns_t)
#print(coll_gffs[0])
#print(conj_coll_gffs[0])

#multiplication:
mult_coll_gffs = np.zeros((len(energies),len(coll_gffs[i])*2-1), dtype=complex) #contains psi(compl.conj.)*psi for each eigenstate i
for i in range(0, len(energies)): #loop through eigenstates
    for k in range(0, len(conj_coll_gffs[i])): #loop through conjugated GFF terms of first factor
        #n = conj_coll_gffs[i][k]
        n = coll_gffs[i][k]
        for h in range (0, len(coll_gffs[i])): #loop through GFF terms of second factor
            m = coll_gffs[i][h]
            if k == 0 and h == 0: #product yielding constant term
                #mult_coll_gffs[i,0] +=n*m
                mult_coll_gffs[i,0] +=n*m
            elif k == 0 and h != 0:
                #mult_coll_gffs[i,h] += n*m
                mult_coll_gffs[i,h] += n*m
            elif k != 0 and h == 0:
                if k % 2 != 0:
                    #mult_coll_gffs[i,k+1] += n*m
                    mult_coll_gffs[i,k] += n*m
                else:
                    #mult_coll_gffs[i,k-1] += n*m
                    mult_coll_gffs[i,k] += n*m
            elif k != 0 and h != 0:
                #if k % 2 != 0: #odd k means negative exponent in e-function (because it is complex. conj.)
                if k % 2 == 0: #odd k means negative exponent in e-function (because it is complex. conj.)
                    if h % 2 != 0: #odd h means positive exponent in e-function
                        if np.ceil(k/2) == np.ceil(h/2):
                            #mult_coll_gffs[i,0] += n*m
                            mult_coll_gffs[i,0] += n*m
                        elif np.ceil(k/2) > np.ceil(h/2):
                            #mult_coll_gffs[i,int(np.ceil(k/2)-np.ceil(h/2))*2] += n*m
                            mult_coll_gffs[i,int(np.ceil(k/2)-np.ceil(h/2))*2] += n*m
                        else:
                            #mult_coll_gffs[i,int(-np.ceil(k/2)+np.ceil(h/2))*2-1] += n*m
                            mult_coll_gffs[i,int(-np.ceil(k/2)+np.ceil(h/2))*2-1] += n*m
                    else: #even h means negative exponent in e-function
                        #mult_coll_gffs[i,int(np.ceil(k/2)+np.ceil(h/2))*2] += n*m
                        mult_coll_gffs[i,int(np.ceil(k/2)+np.ceil(h/2))*2] += n*m
                else: #even k means positive exponent in e-function
                    if h % 2 != 0: #odd h means positive exponent in e-function
                        #mult_coll_gffs[i,int(np.ceil(k/2)+np.ceil(h/2))*2-1] += n*m
                        mult_coll_gffs[i,int(np.ceil(k/2)+np.ceil(h/2))*2-1] += n*m
                    else: #even h means negative exponent in e-function
                        if np.ceil(k/2) == np.ceil(h/2):
                            #mult_coll_gffs[i,0] += n*m
                            mult_coll_gffs[i,0] += n*m
                        elif np.ceil(k/2) > np.ceil(h/2):
                            #mult_coll_gffs[i,int(np.ceil(k/2)-np.ceil(h/2))*2-1] += n*m
                            mult_coll_gffs[i,int(np.ceil(k/2)-np.ceil(h/2))*2-1] += n*m
                        else:
                            #mult_coll_gffs[i,int(-np.ceil(k/2)+np.ceil(h/2))*2] += n*m
                            mult_coll_gffs[i,int(-np.ceil(k/2)+np.ceil(h/2))*2] += n*m

#print(mult_coll_gffs)                            

#integration:
result_coll_gffs = np.zeros((len(energies),len(coll_gffs[i])*2-1),dtype=complex) #contains psi(compl.conj.)*psi for each eigenstate i
for i in range(0, len(energies)): #loop through eigenstates
    for p in range(0, len(mult_coll_gffs[i])): #loop through psi(compl.conj.)*psi and integrate in the following
        if p % 2 != 0:
            result_coll_gffs[i,p] = mult_coll_gffs[i,p]*(1/(np.ceil(p/2)*1j))
        else:
            if p == 0:
                result_coll_gffs[i,p] = mult_coll_gffs[i,p]
            else:
                result_coll_gffs[i,p] = mult_coll_gffs[i,p]*(1/(np.ceil(p/2)*1j*(-1)))
#print(result_coll_gffs)

#residence probability:
theta1 = 2*np.pi #upper bound
theta2 = 0 #lower bound
residence_p = np.zeros(len(energies),dtype=complex)
for i in range(0, len(energies)): #loop through eigenstates
    residence_p[i] += result_coll_gffs[i,0]*(theta1-theta2)
    for q in range(1, len(result_coll_gffs[i])): #loop through integrated[psi(compl.conj.)*psi] coefficents 
        if q % 2 != 0:
            residence_p[i] += result_coll_gffs[i,q]*cmath.exp(1j*np.ceil(q/2)*theta1) - result_coll_gffs[i,q]*cmath.exp(1j*np.ceil(q/2)*theta2)
        else:
            residence_p[i] += result_coll_gffs[i,q]*cmath.exp(-1j*np.ceil(q/2)*theta1) - result_coll_gffs[i,q]*cmath.exp(-1j*np.ceil(q/2)*theta2)

def residence_calcs(theta):
    residence_p_single = np.zeros(len(energies),dtype=complex)
    for i in range(0, len(energies)): #loop through eigenstates
        residence_p_single[i] += result_coll_gffs[i,0]*theta
        for q in range(1, len(result_coll_gffs[i])): #loop through integrated[psi(compl.conj.)*psi] coefficents 
            if q % 2 != 0:
                residence_p_single[i] += result_coll_gffs[i,q]*cmath.exp(1j*np.ceil(q/2)*theta) 
            else:
                residence_p_single[i] += result_coll_gffs[i,q]*cmath.exp(-1j*np.ceil(q/2)*theta)
    return residence_p_single

'''
#transform gff back to coeffs
coeffs_list = []
for i in range(len(energies)):
    coeffs = np.array([])
    coeffs = np.append(coeffs,mult_coll_gffs[i][0].real)
    for k in range(1, len(mult_coll_gffs[i])):
        if k % 2 != 0:
            a = mult_coll_gffs[i][k]
            b = mult_coll_gffs[i][k+1]
            c = (a-b)*1j
            d = (a+b)
            coeffs = np.append(coeffs,c.real)
            coeffs = np.append(coeffs,d.real)
    coeffs_list.append(coeffs)
mult_coll_coeffs = np.vstack(coeffs_list)

mult_coll_coeffs_t = np.transpose(mult_coll_coeffs)
#print(residence_calcs(2*np.pi)[0]-residence_calcs(4/3*np.pi)[0])
#print(residence_calcs(4/3*np.pi)[0]-residence_calcs(2/3*np.pi)[0])
#print(residence_calcs(2/3*np.pi)[0]-residence_calcs(0*np.pi)[0])

norm_mult_coll_coeffs = np.array([mult_coll_coeffs[0]/residence_p[0].real])
'''
###################################################################################

from molmod import deg, kjmol
#print("wfns_t[0] ",wfns_t[0])
def eval_fn(num_levels, a, grid, coeffs):
    """Evaluate the function represented by coeffs
        Arguments:
        | ``grid`` -- the values at which the function must be evaluated
        | ``coeffs`` -- the expansion coefficients
    """
    
    result = np.zeros(grid.shape, float) + coeffs[0]/np.sqrt(a)
    for i in range(num_levels):
        arg = ((i+1)*2*np.pi/a)*grid
        result += (coeffs[2*i+1]/np.sqrt(a/2))*np.cos(arg)
        result += (coeffs[2*i+2]/np.sqrt(a/2))*np.sin(arg)
    return result

#print(wfns[:,0])
import matplotlib.pyplot as pt
#wavefunctions plot
pt.clf()

e2plot=4 #energy level for which the corr. wavefunction is plotted

step = 0.001
x = np.arange(0.0, 2*np.pi*(1+0.5*step), 2*np.pi*step)
v = eval_fn(num_levels, 2*np.pi, x, wfns_t[e2plot])
pt.ylabel('wavefunction')
pt.xlabel('angle [deg]')
pt.plot(x/deg, v, "k-", linewidth=2)
pt.savefig('test_wfns')

#norm_conj_coll_coeffs plot
pt.clf()

step = 0.001
x = np.arange(0.0, 2*np.pi*(1+0.5*step), 2*np.pi*step)
v = eval_fn(num_levels, 2*np.pi, x, conj_coll_coeffs[e2plot])
pt.ylabel('normalised residence density')
pt.xlabel('angle [deg]')
pt.plot(x/deg, v, "k-", linewidth=2)
pt.savefig('test_conj')

#norm_mult_coll_coeffs plot
pt.clf()

step = 0.001
x = np.arange(0.0, 2*np.pi*(1+0.5*step), 2*np.pi*step)
#v = eval_fn(3, 2*np.pi, x, mult_coll_coeffs[0])
v = eval_fn(num_levels, 2*np.pi, x, gff_to_coeffs(mult_coll_gffs)[e2plot])
pt.ylabel('normalised residence density')
pt.xlabel('angle [deg]')
pt.plot(x/deg, v, "k-", linewidth=2)
pt.savefig('test_mult')

#print(len(rotor1.v_coeffs))
#rotor1.v_coeffs = mult_coll_coeffs
rotor1.plot_levels("RotorLevels.png",temperature)


