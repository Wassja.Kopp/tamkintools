# Author:  Wassja A. Kopp
# Version: 1.0
# Date:    24rd Apr 2019

from tamkin import *
from molmod import *
from nasafit import *
from cc import *
from texthermo import *
import matplotlib.pyplot as plt
import numpy
import re

# NpT is used when nothing else is specified (1bar)

import os 
from datetime import date

#------------------------------------------------------------------------------------------------------------------------------------------------

#############
# Constants #
#############

substance = "EPOO1_lowestconf2"

R = 8.3144598
R_a = 8.3144598
kcal = 4.1868

level_prec = 2000
T_plot     = 1000
ExtRotSym  = 1

unit = 2

if unit == 1:
	unit = 1
else:
	unit = kcal

substance_mol  = load_molecule_g03fchk(substance+"_freq.fchk")#, energy=Einf)
substance_nma  = NMA(substance_mol, ConstrainExt())

scana          = load_rotscan_g03log(substance+"_scanOOCC.log")
scanb          = load_rotscan_g03log(substance+"_scanOCCC_merged.log")
scanc          = load_rotscan_g03log(substance+"_scanCCCO.log") 
scand          = load_rotscan_g03log(substance+"_scanCCOC.log")
scane          = load_rotscan_g03log(substance+"_scanCOCC.log")
scanm          = load_rotscan_g03log(substance+"_scanm.log")

rotora         = Rotor(scana,  substance_mol, rotsym=1, cancel_freq="scan", even=False, dofmax=7, num_levels=level_prec)
rotorb         = Rotor(scanb,  substance_mol, rotsym=1, cancel_freq="scan", even=False, dofmax=7, num_levels=level_prec)
rotorc         = Rotor(scanc,  substance_mol, rotsym=1, cancel_freq="scan", even=False, dofmax=7, num_levels=level_prec)
rotord         = Rotor(scand,  substance_mol, rotsym=1, cancel_freq="scan", even=False, dofmax=11,num_levels=level_prec)
rotore         = Rotor(scane,  substance_mol, rotsym=1, cancel_freq="scan", even=False, dofmax=11,num_levels=level_prec)
rotorm         = Rotor(scanm,  substance_mol, rotsym=3, cancel_freq="scan", even=True,  dofmax=3, num_levels=level_prec)

pf_HO   = PartFun(substance_nma, [ExtTrans(), ExtRot(symmetry_number=ExtRotSym)])
pf_HR   = PartFun(substance_nma, [ExtTrans(), ExtRot(symmetry_number=ExtRotSym), rotora, rotorb, rotorc, rotord, rotore, rotorm])

print "rotorOOCC"
printFourier(rotora)
texFourier(rotora, "rotorOOCC", substance)
print "rotorOCCC"
printFourier(rotorb)
texFourier(rotorb, "rotorOCCC", substance)
print "rotorCCCO"
printFourier(rotorc)
texFourier(rotorc, "rotorCCCO", substance)
print "rotorCCOC"
printFourier(rotord)
texFourier(rotord, "rotorCCOC", substance)
print "rotorCOCC"
printFourier(rotore)
texFourier(rotore, "rotorCOCC", substance)
print "rotorm"
printFourier(rotorm)
texFourier(rotorm, "rotorm", substance)

pf_HO.write_to_file(substance+"_HO.csv")
pf_HR.write_to_file(substance+"_HR.csv")

ta             =  ThermoAnalysis(pf_HR, [298.15,300.0,400.0,500.0,600.0,800.0,1000.0,1500.0])
ta.write_to_file("ta_"+substance+".csv")

rotora.plot_levels(substance+"_scana.png",T_plot,level_prec) 
rotorb.plot_levels(substance+"_scanb.png",T_plot,level_prec)
rotorc.plot_levels(substance+"_scanc.png",T_plot,level_prec)
rotord.plot_levels(substance+"_scand.png",T_plot,level_prec)
rotore.plot_levels(substance+"_scand.png",T_plot,level_prec)
rotorm.plot_levels(substance+"_scanm.png",T_plot,level_prec)

TG = 1000
T0 = 298.15
dT1 = 100	# Schrittweite
dT2 = 200	# Schrittweite
#T1 = numpy.array(range(100,TG,100))
#T2 = numpy.array(range(TG,4000,200))
T1 = numpy.array(range(100, TG+dT1, dT1))
T2 = numpy.array(range(TG, 4000+dT2, dT2))
h1 = []
h2 = []
s1 = []
s2 = []

s0 = (pf_HR.entropy(T0)/(joule/(mol*kelvin)))/R_a

hf_subs = -411.007821/(T0*R_a/1000)#hf-nC*hfC-nH*(hfH2/2.)-nO*(hfO2/2.)

# NASA fit calculation
enthalpy, coeffs, lagr_multi, errorfit = nasafit(pf_HR, hf_subs*T0*R_a/1000, T1, T2)

write_tex_dev(substance,enthalpy,unit,R_a*numpy.log(2))

print "\nChi-Squared:\n{:10}{:.8e}\n{:10}{:.8e}".format("Enthalpy:", errorfit[0], "Entropy:", errorfit[1])

#write_tex_CC(substance,substance+"_ccdz.log",substance+"_cctz.log")
write_tex_coeffs(substance,coeffs,numpy.log(2))
write_tex_thermo(substance,hf_subs,T0,s0+numpy.log(2),coeffs,unit)
print_NASA_polynomial_datasheet(substance,pf_HR,numpy.append(T1,T2),coeffs,numpy.log(2))
