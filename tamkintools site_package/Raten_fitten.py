from tamkin import *
import numpy

tunneling = Eckart([pf_ho2,pf_fuel], pf_trans, [pf_prod1, pf_prod2])
km_HRtun = KineticModel([pf_ho2,pf_fuel],pfHR, tunneling) # HR with tunneling
km_HRtun.write_to_file("CSV/kinetic_ethane.csv")
km_HR = KineticModel([pf_ho2,pf_fuel],pfHR) # HR w/o tunneling
km_HOtun = KineticModel([pf_ho2,pf_fuel],pfrrho,tunneling_rrho) # HO with tunneling

T = numpy.array(range(500,2100,100))
k = numpy.zeros((len(T),3),float)
for i in xrange(len(T)):
       k[i,0] = km_HRtun.rate_constant(T[i]) vermutlich noch Division / km_HRtun.unit
       k[i,1] = km_HR.rate_constant(T[i]) vermutlich noch Division / km_HRtun.unit
       k[i,2] = km_HOtun.rate_constant(T[i]) vermutlich noch Division / km_HRtun.unit
B = numpy.ones((len(T),3),float)
B[:,1] = numpy.log(T) # numpy.log =^ ln; numpy.log10 =^ log
B[:,2] = -1./T
log_k = numpy.log(k)
coeffs, residuals, rank, S = numpy.linalg.lstsq(B, log_k, 0.0)
print coeffs
print residuals
print "Hindered Rotator with tunneling: k(T)=",numpy.exp(coeffs[0,0]),"*T^",coeffs[1,0],"*exp(-",coeffs[2,0],"/T)\t[",km_HRtun.unit_name,"]"
print "Hindered Rotator w/o tunneling: k(T)=",numpy.exp(coeffs[0,1]),"*T^",coeffs[1,1],"*exp(-",coeffs[2,1],"/T)\t[",km_HRtun.unit_name,"]"
print "Hindered Oscillator with tunneling: k(T)=",numpy.exp(coeffs[0,2]),"*T^",coeffs[1,2],"*exp(-",coeffs[2,2],"/T)\t[",km_HRtun.unit_name,"]"
print
print "Numerical rate constant values:"
print " T[K]\tk_HRtun(T)\tk_HR(T)\tk_HOtun(T)"
for i in xrange(len(T)):
       print T[i], "\t", k[i,0], "\t", k[i,1], "\t", k[i,2]
