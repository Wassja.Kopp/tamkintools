import sys
from nasafit import *
from datetime import date
import numpy
from molmod import amu
from cc import *

R_a = 8.3144598

############################################################
########    Writing Thermo-Data Deviation files    #########
############################################################

def write_tex_dev(substance,enthalpy,unit,symm=None):
        try:
                Dev_hRT= open(substance+"-Dev_hRT"".tex","w")
                Dev_h  = open(substance+"-Dev_h"".tex","w")
                Dev_s  = open(substance+"-Dev_s"".tex","w")
                Dev_Cp = open(substance+"-Dev_Cp"".tex","w")
        except:
                print("Error")
                sys.exit(0)
        T       = []       # enthalpy[i,0]
        h_NASA  = []       # enthalpy[i,1]
        h_Fit   = []       # enthalpy[i,2]
        hRT_NASA= []
        hRT_Fit = []
        s_NASA  = []       # enthalpy[i,6]
        s_Fit   = []       # enthalpy[i,5]
        Cp_NASA = []       # enthalpy[i,7]
        Cp_Fit  = []       # enthalpy[i,8]
        delta_enthalpy  = []
        delta_enthalpyRT= []
        delta_entropy   = []
        delta_Cp        = []
        # enthalpy is given from nasafit in J, mol, K; desired unit is parameter here
        for i in xrange(len(enthalpy)):
                T.append(enthalpy[i,0])
                h_NASA.append(enthalpy[i,1])  # h/RT from NASA fit w/o kilo
                h_Fit.append(enthalpy[i,2])   # h/RT from computation (to that is fitted)
                delta_enthalpy.append(h_Fit[i]-h_NASA[i]) # delta h already divided by unit
                hRT_NASA.append(h_NASA[i]/1000*T[i]*R_a/unit)
                hRT_Fit.append( h_Fit[i]/1000*T[i]*R_a/unit)
                delta_enthalpyRT.append(hRT_NASA[i]-hRT_Fit[i])
                s_Fit.append((enthalpy[i,5]+symm)/unit)   # s comp from TAMKin in joule/molK
                s_NASA.append((enthalpy[i,6]+symm)/unit)  # s_NASA * R
                delta_entropy.append(s_Fit[i]-s_NASA[i]) # delta s already divided by unit
                Cp_NASA.append(enthalpy[i,7]/unit) # cp_NASA*R
                Cp_Fit.append(enthalpy[i,8]/unit)  # cp from TAMkin in kJ/molK
                delta_Cp.append(Cp_Fit[i]/unit-Cp_NASA[i]/unit)

        if unit == 1:
                # Writing the TeX file (Enthalpy in joule)
                Dev_h.write("{0:^4} {1:^12} {2:^12} {3:^12}".format("%T","h_Fit","h_NASA", "delta h")+"\n")
                Dev_hRT.write("{0:^4} {1:^12} {2:^12} {3:^12}".format("%T","h_Fit","h_NASA", "delta h")+"\n")
                Dev_s.write("{0:^4} {1:^12} {2:^12} {3:^12}".format("%T","s_Fit","s_NASA", "delta s")+"\n")
                Dev_Cp.write("{0:^4} {1:^12} {2:^12} {3:^12}".format("%$T$","$C_{p,\\text{fit}}","$C_{p,\\text{NASA}}", "\\Delta $C_{p}")+"\n")
        else:
                Dev_h.write("{0:^4} {1:^12} {2:^12} {3:^12}".format("%T","h_Fit/kcal","h_NASA/kcal", "(delta h)/kcal")+"\n")
                Dev_hRT.write("{0:^4} {1:^12} {2:^12} {3:^12}".format("%T","h_Fit/kcal","h_NASA/kcal", "(delta h)/kcal")+"\n")
                Dev_s.write("{0:^4} {1:^12} {2:^12} {3:^12}".format("%T","s_Fit/kcal","s_NASA/kcal", "(delta s)/kcal")+"\n")
                Dev_Cp.write("{0:^4} {1:^12} {2:^12} {3:^12}".format("%$T$","$C_{p,\\text{fit}}","$C_{p,\\text{NASA}}", "\\Delta $C_{p}")+"\n")

        for i in xrange(len(enthalpy)):
                Dev_h.write(str(T[i])+" & "+"{0:8.1f}".format(h_Fit[i])+" & "+"{0:8.1f}".format(h_NASA[i])+" & "+"{0:8.3f}".format(delta_enthalpy[i])+"\t"+"\\\\"+"\n")
                Dev_hRT.write(str(T[i])+" & "+"{0:8.1f}".format(hRT_Fit[i])+" & "+"{0:8.1f}".format(hRT_NASA[i])+" & "+"{0:8.3f}".format(delta_enthalpyRT[i])+"\t"+"\\\\"+"\n")
                Dev_s.write(str(T[i])+" & "+"{0:.2f}".format(s_Fit[i])+" & "+"{0:.2f}".format(s_NASA[i])+" & "+"{0:.3f}".format(delta_entropy[i])+"\t"+"\\\\"+"\n")
                Dev_Cp.write(str(T[i])+" & "+"{0:10.2f}".format(Cp_Fit[i])+" & "+"{0:10.2f}".format(Cp_NASA[i])+" & "+"{0:10.3f}".format(delta_Cp[i])+"\t"+"\\\\"+"\n")

#####################
# NASA coefficients #
#####################

def write_tex_coeffs(substance,coeffs,symm=None):
        try:
                Coeff  = open(substance+"-Coeff"".tex","w")
        except:
                print("Error")
                sys.exit(0)

        Coeff.write("{0:^3} {1:^12} {2:^12} {3:^12} {4:^12} {5:^12} {6:^12} {7:^12}".format("%T1","a1","a2","a3","a4","a5","a6","a7"))
        Coeff.write("\n")
        a_I = []
        a_II = []
        head = ""
        # T < 1000K
        for i in xrange(7):
                head = head+"a{}(T < 1000 K),".format(i)
                a_I.append(coeffs[i])
        Coeff.write("\t"+"{0:.4e}".format(a_I[0])+" & "+"{0:.4e}".format(a_I[1])+" & "+"{0:.4e}".format(a_I[2])+" & "+"{0:.4e}".format(a_I[3])+" & "+"{0:.4e}".format(a_I[4])+" & "+"{0:.4e}".format(a_I[5])+" & "+"{0:.4e}".format(a_I[6]+symm)+"\t"+"\\\\"+"\n")
        Coeff.write("{0:^3} {1:^12} {2:^12} {3:^12} {4:^12} {5:^12} {6:^12} {7:^12}".format("%T2","a1","a2","a3","a4","a5","a6","a7")+"\n")
        # T > 1000K
        for i in xrange(7,14):
                head = head+"a{}(T > 1000 K),".format(i-7)
                a_II.append(coeffs[i])
        Coeff.write("\t"+"{0:.4e}".format(a_II[0])+" & "+"{0:.4e}".format(a_II[1])+" & "+"{0:.4e}".format(a_II[2])+" & "+"{0:.4e}".format(a_II[3])+" & "+"{0:.4e}".format(a_II[4])+" & "+"{0:.4e}".format(a_II[5])+" & "+"{0:.4e}".format(a_II[6]+symm)+"\t"+"\\\\"+"\n")
        numpy.savetxt("nasa_coefficients.csv",numpy.reshape(coeffs,(1,len(coeffs))),delimiter=',',header=head)

##############
# CC.tex #
##############

def write_tex_CC(substance,dz,tz):
        try:
                CC = open(substance+"-CC"".tex","w")
        except:
                print("Error opening "+substance+"-CC.tex")
                sys.exit(0)
        name = get_name(substance)
        CC.write("{0:^4} {1:^12} {2:^12} {3:^12} {4:^12} {5:^12}".
          format("%","ccsd(t)","T1", "mp4(sdq)", "mp2", "mp4"))
        CC.write("\n")
        CC.write(name+" dz & "+"\t"+"{0:10.8f}".
          format(checkCC(dz, "CCSD\(T\)\=\s*(\-?\d+\.\d+)D([+-]\d+)"))+" & "+"{0:10.8f}".
          format(checkT1(dz))+" & "+"{0:10.5f}".
          format(checkMP4(dz))+" & "+"{0:10.8f}".
          format(checkMP2(dz))+" & "+"{0:10.8f}".
          format(checkHF(dz))
          +"\t"+"\\\\")
        CC.write("\n")
        CC.write(name+" tz & "+"\t"+"{0:10.8f}".
          format(checkCC(tz, "CCSD\(T\)\=\s*(\-?\d+\.\d+)D([+-]\d+)"))+" & "+"{0:10.8f}".
          format(checkT1(tz))+" & "+"{0:10.5f}".
          format(checkMP4(tz))+" & "+"{0:10.8f}".
          format(checkMP2(tz))+" & "+"{0:10.8f}".
          format(checkHF(tz))
          +"\t"+"\\\\")

##############
# Thermo.tex #
##############

def get_name(substance):
        substcheck1 = re.search("R(\d+)",substance)
        if substcheck1 == None:
                print "No R\(\d+\) found!"
        else:
                name = "\\R{"+substcheck1.group(1)+"} &"
                print "Substance name will be "+name
                #return name

        substcheckO = re.search("R(\d+)(\w)\d*",substance)
        if substcheckO == None:
                print "No R(\d+\) found!"
        else:
                name = "\\ROO["+substcheckO.group(1)+"]{"+substcheckO.group(2)+"} &"
                print "Substance name will be "+name
                #return name

        substcheck3 = re.search("R(\d+)(\w+)\d*H",substance)
        if substcheck3 == None:
                print "No ROOH found!"
        else:
                name = "R"+substcheck3.group(1)+"\ce{"+substcheck3.group(2)+"H} &"
                print "Substance name will be "+name


        substcheck2 = re.search("Q(\d+)O(\d+)H",substance)
        if substcheck2 == None:
                print ""
        else:
                name = "\\QOOH["+substcheck2.group(1)+"]{O} &"
                print "Substance name will be "+name
                #return name

        if substcheck1 == None and substcheckO == None and substcheck2 == None and substcheck3 == None:
                name = "\\ce{"+substance+"} &"
        return name


def write_tex_thermo(substance,hf_subs,T0,sf,coeffs,unit):
        try:
                Thermo = open(substance+"-Thermo"".tex","w")
        except:
                print("Error")
                sys.exit(0)

        name = get_name(substance)

        if unit == 1:
                Thermo.write("{0:^4} {1:^12} {2:^12} {3:^12} {4:^12} {5:^12} {6:^12} {7:^12}".format("%","hf(298.15K)","sf(298.15K)", "Cp(298.15K)", "Cp(500K)", "Cp(1000K)", "Cp(1500K)", "Cp(2000K)"))
        else:
                Thermo.write("{0:^4} {1:^12} {2:^12} {3:^12} {4:^12} {5:^12} {6:^12} {7:^12}".format("%","hf(298.15K)","sf(298.15K)", "Cp_kcal(298.15K)", "Cp_kcal(500K)", "Cp_kcal(1000K)", "Cp_kcal(1500K)", "Cp_kcal(2000K)"))
        Thermo.write("\n")
        #Thermo.write("\\ce{"+substance+"} & "+"\t"+"{0:10.3f}".format(hf_subs*T0*R_a/unit/1000)+" & "+"{0:10.3f}".format(sf*R_a/unit)+" & "+"{0:10.2f}".format(heatcapacity_NASA(298.15,coeffs[0:7])*8.3144598/unit)+" & "+"{0:10.2f}".format(heatcapacity_NASA(500.,coeffs[0:7])*8.3144598/unit)+" & "+"{0:10.2f}".format(heatcapacity_NASA(1000.,coeffs[0:7])*8.3144598/unit)+" & "+"{0:10.2f}".format(heatcapacity_NASA(1500.,coeffs[7:14])*8.3144598/unit)+" & "+"{0:10.2f}".format(heatcapacity_NASA(2000.,coeffs[7:14])*8.3144598/unit)+"\t"+"\\\\")
	Thermo.write(name +"\t"+"{0:10.3f}".format(hf_subs*T0*R_a/unit/1000)+" & "+"{0:10.3f}".format(sf*R_a/unit)+" & "+"{0:10.2f}".format(heatcapacity_NASA(298.15,coeffs[0:7])*8.3144598/unit)+" & "+"{0:10.2f}".format(heatcapacity_NASA(500.,coeffs[0:7])*8.3144598/unit)+" & "+"{0:10.2f}".format(heatcapacity_NASA(1000.,coeffs[0:7])*8.3144598/unit)+" & "+"{0:10.2f}".format(heatcapacity_NASA(1500.,coeffs[7:14])*8.3144598/unit)+" & "+"{0:10.2f}".format(heatcapacity_NASA(2000.,coeffs[7:14])*8.3144598/unit)+"\t"+"\\\\")

###############################################################
# Nasa polynomial datasheet needs some molecular informations #
###############################################################

def print_NASA_polynomial_datasheet(substance,pf_subs,T,coeffs,symm=None):
        formula = pf_subs.chemical_formula
        O = " "
        H = " "
        C = " "
        oxy = " "
        hydro = " "
        carb = " "
        other = " "
        oth = " "
        chars = list(formula)
        for i in xrange(len(chars)):
                if chars[i] == "O":
                        O = "O"
                        if chars[i+1].isdigit():
                                oxy = int(chars[i+1])
                        else:
                                oxy = 1
                elif chars[i] == "H":
                        H = "H"
                        if chars[i+1].isdigit():
                                hydro = int(chars[i+1])
                        else:
                                hydro = 1
                elif chars[i] == "C":
                        C = "C"
                        if chars[i+1].isdigit():
                                carb = int(chars[i+1])
                        else:
                                carb = 1
        print ""
#print "Standard Format for Nasa-Polynomials (note high temperature coefficients coming first):"
#print "C2H5COCH3  29/4/15 THERMC   4H   8O   1    0G   300.000  5000.000 1380.000    31"
#print " 1.25141235E+01 1.84604475E-02-6.14816056E-06 9.37968198E-10-5.37721162E-14    2"
#print "-3.46118541E+04-3.82309512E+01 1.92048232E+00 4.00983271E-02-2.20686090E-05    3"
#print " 5.85110392E-09-5.66573305E-13-3.05908852E+04 1.98194682E+01                   4"
#print ""
        print "THERMO"
        print "{:10.3f}{:10.3f}{:10.3f}".format(min(T), 1000, max(T))
#print "! NASA Polynomial format for CHEMKIN-II"
        today = date.today()
        insertdate = str(today.day) + "/" + str(today.month) + "/" + str(today.year - 2000)
        print "{:11}{:8}{:5}{:1}{:4}{:1}{:4}{:1}{:4}{:1}{:4}{:4}{:<9.3f}{:<9.3f}{:<9.3f}{:>5}".format(formula, insertdate, "MBFD",C, carb, H, hydro, O, oxy, other, oth, "G", min(T), max(T), 1000, "1")
        print "{: .8E}{: .8E}{: .8E}{: .8E}{: .8E}    2".format(coeffs[7], coeffs[8], coeffs[9], coeffs[10], coeffs[11])
        print "{: .8E}{: .8E}{: .8E}{: .8E}{: .8E}    3".format(coeffs[12], coeffs[13]+symm, coeffs[0], coeffs[1], coeffs[2])
        print "{: .8E}{: .8E}{: .8E}{: .8E}                   4".format(coeffs[3], coeffs[4], coeffs[5], coeffs[6]+symm)

#########
# Plots #
#########

def plotcomp(f_Fit,f_NASA,fname1,fname2,keyword):
        f0 = plt.figure()
        ax0 = f0.add_subplot(111)
        ax0.plot(T, f_Fit, linestyle='-', color='r', linewidth=2, label=fname1)
        ax0.plot(T, f_NASA, linestyle='--', color='b', linewidth=2, label=fname2)
        plt.xlabel('Temperature / [K]')
        if keyword == 'Enthalpy_kcal':
                plt.ylabel('Enthalpy / [kcal/mol]')
        elif keyword == 'Entropy_cal':
                plt.ylabel('Entropy / [cal/molK]')
        elif keyword == 'HeatCapacity_cal':
                plt.ylabel('Heat capacity / [cal/molK]')
        elif keyword == 'Enthalpy':
                plt.ylabel('Enthalpy / [kJ/mol]')
        elif keyword == 'Entropy':
                plt.ylabel('Entropy / [J/molK]')
        elif keyword == 'HeatCapacity':
                plt.ylabel('Heat capacity / [J/molK]')
        plt.title('Comparison of f_Fit and f_NASA')
        ax0.set_xlim([min(T), max(T)])
        ax0.set_ylim([min(np.concatenate((f_Fit,f_NASA), axis=0))-5, max(np.concatenate((f_Fit,f_NASA), axis=0))+5])
        ax0.grid(True)
        plt.legend(loc='best', borderaxespad=2)
        plt.savefig('RH_'+keyword+'.eps', format='eps', dpi=300)
        plt.savefig('RH_'+keyword+'.png', format='png', dpi=300)


################
# Fourier fits #
################

def printFourier(RotorObject):
        
#           The basis functions are:
#            | 1/sqrt(a/2)
#            | cos(2*pi*x/a)/sqrt(a/2)
#            | sin(2*pi*x/a)/sqrt(a/2)
#            | ...
#            | cos(nmax*2*pi*x/a)/sqrt(a/2)
#            | sin(nmax*2*pi*x/a)/sqrt(a/2)

        print "Number of coeffs: "+str(len(RotorObject.v_coeffs))
        for i in range(len(RotorObject.v_coeffs)):
                if(RotorObject.v_coeffs[i] != 0 and i==0):
                        sys.stdout.write(str(RotorObject.v_coeffs[i]*2625.4995)+'/sqrt('+str(RotorObject.hb.a)+'/2)')
                if(RotorObject.v_coeffs[i] != 0 and i>0):
                        sys.stdout.write('+1/sqrt('+str(RotorObject.hb.a)+'/2)*'+str(RotorObject.v_coeffs[i]*2625.4995)+'*')
                        if(i%2 == 0):
                                sys.stdout.write('sin(') 
                        else:
                                sys.stdout.write('cos(')
                        sys.stdout.write('2*PI()/'+str(RotorObject.hb.a)+'*'+str(numpy.ceil(i/2.))+'*x)')
        sys.stdout.write("\n")
        sys.stdout.flush()

def texFourier(RotorObject, name, substance):

#           The basis functions are:
#            | 1/sqrt(a/2)
#            | cos(2*pi*x/a)/sqrt(a/2)
#            | sin(2*pi*x/a)/sqrt(a/2)
#            | ...
#            | cos(nmax*2*pi*x/a)/sqrt(a/2)
#            | sin(nmax*2*pi*x/a)/sqrt(a/2)

# THIS IS FOR a = 2*PI !!!!!!!!

        try:
                Fourier = open(name+".tex","w")
        except:
                print("texFourier: Error opening "+name+".tex for writing")
                sys.exit(0)

        print "Number of coeffs: "+str(len(RotorObject.v_coeffs))
        Fourier.write("$")
        for i in range(len(RotorObject.v_coeffs)):
                if(RotorObject.v_coeffs[i] != 0 and i==0):
                        Fourier.write('{:7.5f}'.format(RotorObject.v_coeffs[i]*2625.4995)+'/\sqrt{\pi}')
                if(RotorObject.v_coeffs[i] != 0 and i>0):
                        Fourier.write('+1/\sqrt{\pi}*'+'{:7.5f}'.format(RotorObject.v_coeffs[i]*2625.4995)+'*')
                        if(i%2 == 0):
                                Fourier.write('\sin(')
                        else:
                                Fourier.write('\cos(')
                        Fourier.write('{:2.0f}'.format(numpy.ceil(i/2.))+'\\theta)')
        Fourier.write("$\n")
        Fourier.write("Reduced moment of inertia around dihedral "+str(RotorObject.rot_scan.dihedral[0]+1)+","+str(RotorObject.rot_scan.dihedral[1]+1)+","+str(RotorObject.rot_scan.dihedral[2]+1)+","+str(RotorObject.rot_scan.dihedral[3]+1)+": I="+str(RotorObject.reduced_moment/amu)+" [amu*bohr**2]")
        Fourier.flush()
        Fourier.close()

