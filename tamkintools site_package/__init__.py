# -*- coding: utf-8 -*-

"""TAMkin is a post-processing toolkit for normal mode, thermochemistry and kinetics."""

from .version import __version__

from tamkintools.optimized_rotor import *
from tamkintools.combine_scans import *
