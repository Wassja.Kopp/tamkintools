# Modification module for an optimized fit
#
# This file is NOT part of TAMkin.
#

import tamkin
from tamkin import *
from tamkintools import *
from tamkintools import combine_scans as cs

from tamkin.nma import NMA, MBH
from tamkin.geom import transrot_basis

from molmod import deg, kjmol, angstrom, centimeter, amu, boltzmann, \
    lightspeed, cached

import numpy as np

import matplotlib.pyplot as pt

import cmath
color_added_points = 'g'
color_normal_points = 'r'
#color_erased_points = 
marker_normal = 'x'
marker_added = 'o'

__all__ = [
    "Opt_Rotor"
]

class Opt_Rotor(Rotor):
    """
    insert explanation
    """
    def __init__(self, rot_scan, Molecule=None, cancel_freq='mbh',even=False, rotsym=1, num_levels=100, derivatives=None, weights=None, dofmax=6, setnma_angle=None):
        
        
        self.rot_scan=rot_scan

        # finding out the number of points in rot_scan and saving it in number_rot_scan
        number_rot_scan=len(rot_scan.potential[0])

        self.Molecule=Molecule
        self.cancel_freq=cancel_freq
        self.cancel_freq_arg=cancel_freq
        self.even=even
        self.rotsym=rotsym
        self.num_levels=num_levels
        self.dofmax=dofmax
        self.setnma_angle=setnma_angle

        # the user can choose to hand over specific weights, else the default value will be 1 for each scan point
        if weights==None:
            list_weights =[]
            for i in range(0,number_rot_scan):
                list_weights += [1]
            self.weights=list_weights
        else:
            self.weights=weights

            
        self.derivatives=derivatives

        # initilizing the original Rotor
        Rotor.__init__(self, rot_scan=rot_scan, molecule=Molecule, cancel_freq=cancel_freq, rotsym=rotsym, num_levels=num_levels, dofmax=dofmax, even=self.even)
        
        

    def init_part_fun(self, nma, partf):

        # defining nma_angle
        from molmod.ic import dihed_angle
        self.nma_angle = dihed_angle(nma.coordinates[self.rot_scan.dihedral])[0]
        '''
        # defining nma_angle
        from molmod.ic import dihed_angle
        if self.setnma_angle == None:
            self.nma_angle = dihed_angle(nma.coordinates[self.rot_scan.dihedral])[0]
        else:
            self.nma_angle = self.setnma_angle
        '''
        print('nma angle in opt_rotor is: ' +str(self.nma_angle*180/np.pi))
        # building the temporary scan 'scantemp'
        angles = [self.nma_angle, self.nma_angle + (np.pi/2), self.nma_angle + np.pi, self.nma_angle - (np.pi/2)]
        energies = [0.0,0.0006,0.001,0.0006]
        scantemp = tamkin.data.RotScan(dihedral=self.rot_scan.dihedral, top_indexes=self.rot_scan.top_indexes, potential=[angles, energies])

        # making a temporary Rotor with scantemp
        RotorTemp=Rotor(scantemp, Molecule, cancel_freq=self.cancel_freq, even=False, num_levels=self.num_levels, dofmax=14)

        # init_part_fun of original rotor.py with scantemp
        RotorTemp.init_part_fun(nma, partf)
        angles, energies = self.potential

        # overwriting v_coeffs with the opt_fit approach
        RotorTemp.v_coeffs=self.opt_fit(angles, energies, self.dofmax, rotsym=RotorTemp.rotsym, even=self.even, v_threshold=self.v_threshold,  num_levels=self.num_levels, weights = self.weights, derivatives = self.derivatives, size= RotorTemp.hb.size)
        RotorTemp.dofmax=self.dofmax

        # solving the Schroedinger equation again for changed potential
        RotorTemp.energy_levels,RotorTemp.wfns=RotorTemp.hb.solve(RotorTemp.moment, RotorTemp.v_coeffs, evecs=True)
    
        # energy_levels[:num_levels] makes a new list out of energy_levels from index 0 to num_levels
        RotorTemp.energy_levels=RotorTemp.energy_levels[:RotorTemp.num_levels]
        
        # after making the Rotor object 'RotorTemp' with scantemp, the real scan 'rot_scan' is now inserted
        RotorTemp.rot_scan=self.rot_scan
        RotorTemp.potential=self.potential

        # replacing self with RotorTemp
        self.__dict__.update(RotorTemp.__dict__)
        if self.setnma_angle != None:
            self.nma_angle = self.setnma_angle
        
        # updating several attributes of self
        # the cancelation frequency based on the scan
        if RotorTemp.hb is None:
            if not isinstance(RotorTemp.cancel_freq, float):
                raise ValueError('No cancelation frequency was computed for rotor "%s"' % self.name)
        else:
            self.force_constant = RotorTemp.hb.eval_deriv2(np.array([self.nma_angle]), RotorTemp.v_coeffs)[0]
            if self.force_constant < 0:
                self.scan_cancel_freq = np.sqrt(abs(self.force_constant)/RotorTemp.moment)/(2*np.pi)
                print('WARNING: The force_constant is negative')
            else:
                self.scan_cancel_freq = np.sqrt(self.force_constant/RotorTemp.moment)/(2*np.pi)
            if self.cancel_freq_arg == 'scan':
                self.cancel_freq = self.scan_cancel_freq
            elif abs(self.cancel_freq - self.scan_cancel_freq) > 0.3*abs(self.cancel_freq):
                print('WARNING: The cancelation frequency of rotor "%s" obtained with MBH (%.1f cm^-1) deviates a lot from the one derived from the scan (%.1f cm^-1).' % (
                    self.name, self.cancel_freq/(lightspeed/centimeter), self.scan_cancel_freq/(lightspeed/centimeter)
                ))
        
        # print a warning is the cancelation frequency is rather high.
        if self.cancel_freq > 500*(lightspeed/centimeter):
            print('WARNING: the cancelation frequency of rotor "%s" is rather high: %.1f cm^-1.' % (
                self.name, self.cancel_freq/(lightspeed/centimeter)
            ))
        elif self.cancel_freq <= 0:
            print('WARNING: the cancelation frequency of rotor "%s" is negative: %.1f cm^-1.' % (
                self.name, self.cancel_freq/(lightspeed/centimeter)
            ))

        # scaling factors
        self.freq_scaling = partf.vibrational.freq_scaling
        self.zp_scaling = partf.vibrational.zp_scaling
        self.classical = partf.vibrational.classical
        
        # no special treatment for large_fixed here. 
        self.moment=RotorTemp.moment
        self.reduced_moment=RotorTemp.reduced_moment
        

    def opt_fit(self, grid, v, dofmax=50, rotsym=1, even=False, v_threshold=1000.0,  num_levels=100, weights = None, derivatives = None, minimize_curvature=1, rcond=0.0, size= None):
        global curvature_length
        curvature_length = 1000
        """Fit the expansion coefficients that represent function f

           Arguments:
            | ``grid`` -- The x values on which the function f is known.
            | ``v`` -- The function to be represented by expansion coefficients.
            | ``dofmax`` -- The maximum number of cosines in the fit. When
                            even==False, the same number of sines is also
                            included.
            | ``weights`` -- weights is an array with values between 1 and 0 and 
                             the length of the number of scan points. This allows
                             the scan points to be weighted differently for the fit.
            | ``derivatives`` -- 
            | ``minimize_curvature`` -- equals 1 for the optimized fit modification


           Optional arguments:
            | ``rotsym`` -- Impose this rotational symmetry [default=1]. Must be
                            an integer and is least 1..
            | ``even`` -- Only fit even functions, i.e. cosines. [default=False]
            | ``rcond`` -- The cutoff for the singular values in the least
                           squares fit. [default=1e-10]
            | ``v_threshold`` -- Tolerance on the relative error between the
                                 Fourier expansion and the data points of the
                                 scan. [default=0.01]. Absolute errors smaller
                                 than 1 kJ/mol are always ignored.

           In case the Fourier expansion represents a poor fit (determined by
           v_threshold), a ValueError is raised. It means that you have to
           check your torsional scan datapoints for errors.
        """
        #import matplotlib.pyplot as pt
        if rotsym < 1:
            raise ValueError("rotsym must be at least 1.")
          
        # construct the design matrix
        ncos = int(min(dofmax, num_levels/rotsym))
        if derivatives != None:
                [derivative_values, derivative_positions, derivative_weights] = derivatives
        else:
                [derivative_values, derivative_positions, derivative_weights] =[None, None, None]
        if derivative_values == None:
                derivative_length = 0
        else:
            derivative_length = len(derivative_values)
            # construct the design matrix
            ncos = int(min(dofmax, num_levels/rotsym))
        
            minimize_curvature = 1
            # curvature_length = 1000 chosen arbitrarily showing good results
            curvature_length = 1000
        if even:
            A = np.zeros((len(grid)+derivative_length+minimize_curvature*curvature_length, ncos+1), float)
        else:
            A = np.zeros((len(grid)+derivative_length+minimize_curvature*curvature_length, int(2*ncos+1)), float)
        
        # Fill matrix A with values
        A[0:len(grid),0] = weights/np.sqrt(2*np.pi)
        counter = 1
        for i in range(ncos):
            arg = ((i+1)*rotsym*2*np.pi/(2*np.pi))*grid
            A[0:len(grid),counter] = np.cos(arg)/np.sqrt((2*np.pi)/2)*weights
            counter += 1
            if not even:
                A[0:len(grid),counter] = np.sin(arg)/np.sqrt((2*np.pi)/2)*weights
                counter += 1
        #Fill matrix A with derivatives
        #A[0:len(grid),0] = 1.0/np.sqrt((2*np.pi)) - > derivative of constant is 0
        if derivative_length > 0:
                counter = 1
                derivative_positions = np.asarray(derivative_positions)
                print("opt_fit: Fixing derivatives at "+str(derivative_positions))
                for i in range(ncos):
                    factor = rotsym*(2*np.pi)*(i+1)/(2*np.pi)
                    arg = ((i+1)*rotsym*(2*np.pi)/(2*np.pi))*derivative_positions

                    A[(len(grid)):(len(grid)+derivative_length),counter] = -factor*np.sin(arg)/np.sqrt((2*np.pi)/2)*derivative_weights
                    print("Argument "+str(i)+" is "+str(arg)+"; A_"+str(i)+"="+str(                    A[(len(grid)):(len(grid)+derivative_length),counter]))
                    counter += 1
                    if not even:
                        A[(len(grid)):(len(grid)+derivative_length),counter] = factor*np.cos(arg)*(i+1)/np.sqrt((2*np.pi)/2)*derivative_weights
#                    A[(len(grid)):(len(grid)+derivative_length),counter] = factor*np.cos(arg)/np.sqrt((2*np.pi)/2)*derivative_weights
                        counter += 1
        
        if minimize_curvature:
                
                curvature_grid = np.linspace(0.0,2*np.pi, curvature_length)
                curvature_grid = np.asarray(curvature_grid)
                counter = 1
                for i in range(ncos):
                        # weight factor at 0.001 chosen arbitrarily showing good results
                        factor = 0.001*(rotsym*2*np.pi*(i+1)/(2*np.pi))**2
                        arg = ((i+1)*rotsym*2*np.pi/(2*np.pi))*curvature_grid
                        A[len(grid)+derivative_length:(len(grid)+derivative_length+curvature_length),counter] = -factor*np.cos(arg)/np.sqrt((2*np.pi)/2)
                        counter +=1
                        if not even:
                                A[(len(grid))+derivative_length:(len(grid)+derivative_length+curvature_length),counter] = -factor*np.sin(arg)/np.sqrt((2*np.pi)/2)
                                counter += 1
                #print(len(A))
        

        v_full = np.zeros(len(v)+derivative_length+minimize_curvature*curvature_length)
        #for i in range(0,len(v)):
         #   print('point' +str(i) +'of energy' +str((v[i])*2625.4995) + 'has corresponding weghts' +str(weights[i]))
        v_full[0:len(v)] = v*weights;
        v_full[len(v):len(v)+derivative_length] = derivative_values
        v_full[len(v)+derivative_length:len(v)+derivative_length+minimize_curvature*curvature_length]  = np.zeros(minimize_curvature*curvature_length, float)
#        import sys
#        np.set_printoptions(threshold=sys.maxsize)
#        print("-- Matrix A --")
#        print(str(A))
#        print("-- Vector v --")
#        print(str(v_full))
#        for i in len(v_full)
#            print("v_"+str(i)+": "+str(v_full[i])

        coeffs, residuals, rank, S = np.linalg.lstsq(A, v_full, rcond)

        # check the error
        residual = np.dot(A, coeffs) - v_full
        rmsd = (residual**2).mean()**0.5
        rms = (v_full**2).mean()**0.5
        abs_threshold = max(1*kjmol, rms*v_threshold)
        if (rmsd > abs_threshold).any():
            raise ValueError("Residual is too large (v_th=%f). (poor Fourier expansion.) rmsd [kJ/mol] = %f, rms [kJ/mol] = %f" % (v_threshold, rmsd/kjmol, rms/kjmol))

        # collect the parameters in a convenient array
        result = np.zeros(size)
        result[0] = coeffs[0]

        if even:
            tmp = result[2*rotsym-1::2*rotsym]
            tmp[:ncos] = coeffs[1:]
        else:
            tmp = result[2*rotsym-1::2*rotsym]
            tmp[:ncos] = coeffs[1::2]
            tmp = result[2*rotsym-0::2*rotsym]
            tmp[:ncos] = coeffs[2::2]
        arclength = calculateArcLength(self, coeffs)
        curvature = calculateAverageCurvature(self, coeffs)
        directArclength = 0.0
        for i in range(0, len(grid)-1):
            directArclength += np.sqrt( (v[i]-v[i+1])**2+(grid[i]-grid[i+1])**2)
        return result

    def plot_levels_all(self, prefix, temp, do_levels=True, do_data=True,erased_points = [],added_points=[]):
        """Plots the potential with the energy levels

           Arguments:
            | ``prefix`` -- A filename prefix for the png files.
            | ``temp`` -- A temperature that is used to indicate the statistical
                          weight of each level in the plots

           Optional argument:
            | ``do_levels`` -- When True, the energy levels are plotted.
                               [default=True]
            | ``do_data`` -- When True, the data points are plotted.
                             [default=True]

           One image will be generated:
            | ``${prefix}.png`` -- The potential and the energy levels.
        """
        import matplotlib.pyplot as pt
        pt.clf()
        if do_data:
           # if len(added_points) > 0:
            #    added_angles,added_energies = added_points
             #   a = 2*np.pi
             #   added_angles -= np.floor(np.asarray(added_angles)/a)*a
             #   ref_energy = cs.potential_reference(self.rot_scan, self.nma_angle)
             #   relative_energies = added_energies-ref_energy
             #   print('added points in opt_rotor is: ' + str(added_points))
             #   leng_ap = len(added_points)
             #   for leng in range (leng_ap ):
                    #pt.plot(np.asarray(added_angles)[leng]/deg, (np.asarray(added_energies)[leng]-ref_energy)/kjmol, 'gx') ## New line for added points
                    #pt.plot.lines = [] 
             #       pt.plot(added_angles[leng]/deg, np.asarray(relative_energies)[leng]/kjmol, color='orange', marker = marker_added,mew=2)
            # plot the original potential data 
            #added_angles,added_energies = added_points
            if self.rot_scan.potential is not None:
                #added_angles,added_energies = added_points
                angles, energies = self.potential
                leng_ap = len(added_points[0])
                angle = len(angles)
                orig_scan_leng = angle-leng_ap
                if len(added_points)>0:
                    for count in range(angle):	
                        if  count < orig_scan_leng:
                        
                            pt.plot(angles/deg, energies/kjmol, 'bx', marker= marker_normal,mew=2)
                        else:
                            a = 2*np.pi
                            added_angles,added_energies = added_points
                            added_angles -= np.floor(np.asarray(added_angles)/a)*a
                            ref_energy = cs.potential_reference(self.rot_scan, self.nma_angle)
                            relative_energies = added_energies-ref_energy
                            for leng in range (leng_ap ):
                                pt.plot(added_angles[leng]/deg, np.asarray(relative_energies)[leng]/kjmol, color='orange', marker= marker_added,mew=2)
		   
                    
      
            if len(erased_points) > 0:
             
                erased_angles,erased_energies = erased_points
                a = 2*np.pi
                erased_angles -= np.floor(np.asarray(erased_angles)/a)*a
                ref_energy = cs.potential_reference(self.rot_scan, self.nma_angle)
                #print('ref_energies are: ' +str(ref_energy))
                #print('relative energies in kjmol are: ' +str(ref_energy))
                relative_energies = erased_energies-ref_energy
                #print('relative enrgies are: ' + str(relative_energies))
                #print('Erased energies are: ' +str(erased_energies))	
                leng_ep = len(erased_points[0]) ## changed the length
                #print('length of erased Points are: ' +str(leng_ep))
                for leng in range(leng_ep):
                    pt.plot(np.asarray(erased_angles)[leng]/deg,np.asarray(relative_energies)[leng]/kjmol,'#5D3A9B',marker = marker_added,mew =2) ## New line for erased points
                    #print('erased angles in for loop are: ' +str(np.asarray(erased_angles)[leng]/deg))
                    #print('erased angles in for loop are: ' +str(np.asarray(relative_energies)[leng]/kjmol))

                   
            pt.axvline(self.nma_angle/deg, color="silver")
        # plot the fitted potential
        if self.hb is not None:
            step = 0.001
            x = np.arange(0.0, 2*np.pi*(1+0.5*step), 2*np.pi*step)
            v = self.hb.eval_fn(x, self.v_coeffs)
            pt.plot(x/deg, v/kjmol, "k", linewidth=2)
        if do_levels:
            # plot the energy levels
            eks = self.energy_levels/(temp*boltzmann)
            bfs = np.exp(-eks)
            bfs /= bfs.sum()
            for i in range(self.num_levels):
                e = (self.energy_levels[i])/kjmol
                pt.axhline(e, color="k", linewidth=0.5)
                pt.axhline(e, xmax=bfs[i], color="b", linewidth=2)
        if self.rot_scan.potential is not None:
            angles, energies = self.potential
            pt.ylim(
                energies.min()/kjmol,
                1.5*energies.max()/kjmol
            )
            fit_energies = self.hb.eval_fn(angles, self.v_coeffs)
            rmsd = ((fit_energies - energies)**2).mean()**0.5
            rms = (energies**2).mean()**0.5
            rrmsd = rmsd/rms
            title = 'RMSD [kJ/mol] = %.1f    RRMSD [%%] = %.0f    dofmax = %i   rotsym = %i' % (
               rmsd, rrmsd*100, self.dofmax, self.rotsym
            )
            if self.even:
                title += '   even'
            #pt.title(title)
        pt.xlim(0, 360)
        pt.ylabel("Energy [kJ/mol]")
        pt.xlabel("Dihedral angle [deg]")
        pt.savefig(prefix,dpi=300)

    '''
wfns[i]format:
[const.    sin1    cos1    sin2  cos2    sin3 usw

gff format:
[const. exp(1j*theta) exp(-1j*theta) exp(2j*theta) exp(-2j*theta) usw.

transformation:
a*sin(theta)+b*cos(theta)=(a/(2j)+b/2)*exp(1j*theta)+(-a/(2j)+b/2)*exp(-1j*theta)
    '''
    def coeffs_to_gff(self,wfns):
        gff = np.array([]) 
        # second index of wfns_t is terms of fourier series
        gff = np.append(gff,wfns[0]) #constant term
        for k in range(1, len(wfns)):
            if k % 2 != 0:            #index 1,3,5....
                a = wfns[k]   #coeff cos
                b = wfns[k+1] #coeff sin
                c = (b/(2j))+(a/2) #1/2*(a-ib)
                d = (-b/(2j))+(a/2)#1/2*(a+ib)
                gff = np.append(gff,c)
                gff = np.append(gff,d)
        return gff
#print("coll_gffs ",coll_gffs)
#print(wfns[0])

# Translates GFF terms to Fourier series coeffs. 
    def gff_to_coeffs(self,gffs):
        coeffs = np.array([])
        coeffs = np.append(coeffs,gffs[0].real) #constant term
        for k in range(1, len(gffs)):
            if k % 2 != 0:            #index 1,3,5....
                a = gffs[k]        #positive exponent
                b = gffs[k+1]      #negative exponent
                c = (a+b)             #coeff for cos
                d = (a-b)*1j          #coeff for sin
                coeffs = np.append(coeffs,c.real)
                coeffs = np.append(coeffs,d.real)
        return coeffs    


#conjugation:
# for this we create with "conj_coll_gffs" a new gff that is the conjugate version of coll_gffs
    def conj_gff(self,coll_gffs):
        conj_coll_gffs = np.conj(coll_gffs) #in addition all positive and negative orders are switched, keep that in mind
#print(conj_coll_gffs)
#        conj_coll_coeffs = self.gff_to_coeffs(conj_coll_gffs)
        return conj_coll_gffs

#multiplication by complex conjugate:
    def mult_gff(self,coll_gffs):
        mult_coll_gffs = np.zeros(len(coll_gffs)*2-1, dtype=complex) #contains psi(compl.conj.)*psi
#for i in range(0, len(energies)): #loop through eigenstates
        conj_coll_gffs = self.conj_gff(coll_gffs)
        for k in range(0, len(conj_coll_gffs)): #loop through conjugated GFF terms of first factor
            n = coll_gffs[k]
            for h in range (0, len(coll_gffs)): #loop through GFF terms of second factor
                m = coll_gffs[h]
                if k == 0 and h == 0: #product yielding constant term
                    #mult_coll_gffs[i,0] +=n*m
                    mult_coll_gffs[0] +=n*m
                elif k == 0 and h != 0:
                    #mult_coll_gffs[i,h] += n*m
                    mult_coll_gffs[h] += n*m
                elif k != 0 and h == 0:
                    if k % 2 != 0:
                        #mult_coll_gffs[i,k+1] += n*m
                        mult_coll_gffs[k] += n*m
                    else:
                        #mult_coll_gffs[i,k-1] += n*m
                        mult_coll_gffs[k] += n*m
                elif k != 0 and h != 0:
                    #if k % 2 != 0: #odd k means negative exponent in e-function (because it is complex. conj.)
                    if k % 2 == 0: #odd k means negative exponent in e-function (because it is complex. conj.)
                        if h % 2 != 0: #odd h means positive exponent in e-function
                            if np.ceil(k/2) == np.ceil(h/2):
                                #mult_coll_gffs[i,0] += n*m
                                mult_coll_gffs[0] += n*m
                            elif np.ceil(k/2) > np.ceil(h/2):
                                #mult_coll_gffs[i,int(np.ceil(k/2)-np.ceil(h/2))*2] += n*m
                                mult_coll_gffs[int(np.ceil(k/2)-np.ceil(h/2))*2] += n*m
                            else:
                                #mult_coll_gffs[i,int(-np.ceil(k/2)+np.ceil(h/2))*2-1] += n*m
                                mult_coll_gffs[int(-np.ceil(k/2)+np.ceil(h/2))*2-1] += n*m
                        else: #even h means negative exponent in e-function
                            #mult_coll_gffs[i,int(np.ceil(k/2)+np.ceil(h/2))*2] += n*m
                            mult_coll_gffs[int(np.ceil(k/2)+np.ceil(h/2))*2] += n*m
                    else: #even k means positive exponent in e-function
                        if h % 2 != 0: #odd h means positive exponent in e-function
                            #mult_coll_gffs[i,int(np.ceil(k/2)+np.ceil(h/2))*2-1] += n*m
                            mult_coll_gffs[int(np.ceil(k/2)+np.ceil(h/2))*2-1] += n*m
                        else: #even h means negative exponent in e-function
                            if np.ceil(k/2) == np.ceil(h/2):
                                #mult_coll_gffs[i,0] += n*m
                                mult_coll_gffs[0] += n*m
                            elif np.ceil(k/2) > np.ceil(h/2):
                                #mult_coll_gffs[i,int(np.ceil(k/2)-np.ceil(h/2))*2-1] += n*m
                                mult_coll_gffs[int(np.ceil(k/2)-np.ceil(h/2))*2-1] += n*m
                            else:
                                #mult_coll_gffs[i,int(-np.ceil(k/2)+np.ceil(h/2))*2] += n*m
                                mult_coll_gffs[int(-np.ceil(k/2)+np.ceil(h/2))*2] += n*m
        return mult_coll_gffs
#print(mult_coll_gffs)                            

#integration:
    def int_gff(self, coll_gffs):
        result_coll_gffs = np.zeros(len(coll_gffs)*2-1,dtype=complex) #contains psi(compl.conj.)*psi
        mult_coll_gffs = self.mult_gff(coll_gffs)
        for p in range(0, len(mult_coll_gffs)): #loop through psi(compl.conj.)*psi and integrate in the following
            if p % 2 != 0:
                result_coll_gffs[p] = mult_coll_gffs[p]*(1/(np.ceil(p/2)*1j))
            else:
                if p == 0:
                    result_coll_gffs[p] = mult_coll_gffs[p] # this constant term becomes linear!!!
                else:
                    result_coll_gffs[p] = mult_coll_gffs[p]*(1/(np.ceil(p/2)*1j*(-1)))
        return result_coll_gffs
      
    def residence_probability_GFF(self, thetas, e2plot): # wrong!
      
        energies, wfns = self.hb.solve(self.reduced_moment, self.v_coeffs, evecs=True)
        wfns_t = np.transpose(wfns)

        gff  = self.coeffs_to_gff(wfns_t[e2plot])
        mult = self.mult_gff(gff)
        result_coll_gffs = self.int_gff(mult)
      
        P = np.zeros(len(thetas),dtype=float)
        for i in range(len(P)):
            # the constant term becomes linear
            if thetas[i] < thetas[i-1]:# periodic boundary!
                P[i] += result_coll_gffs[0]*(thetas[i]-0)
                print("P["+str(i)+"] = "+str(result_coll_gffs[0])+"*("+str(thetas[i])+" - 0)")
                P[i] += result_coll_gffs[0]*(2*np.pi-thetas[i-1])
                print("P["+str(i)+"] = "+str(result_coll_gffs[0])+"*(2pi - "+str(thetas[i-1])+")")
            else:
                print("P["+str(i)+"] = "+str(result_coll_gffs[0])+"*("+str(thetas[i])+" - "+str(thetas[i-1])+")")
                P[i] = result_coll_gffs[0]*(thetas[i]-thetas[i-1])
            # now from order 1 onwards:
            for q in range(1,len(result_coll_gffs)): #loop through integrated[psi(compl.conj.)*psi] coefficents 
                if q % 2 != 0:
                    P[i] += result_coll_gffs[q]*cmath.exp(1j*np.ceil(q/2)*thetas[i]) - result_coll_gffs[q]*cmath.exp(1j*np.ceil(q/2)*thetas[i-1])
                else:
                    P[i] += result_coll_gffs[q]*cmath.exp(-1j*np.ceil(q/2)*thetas[i]) - result_coll_gffs[q]*cmath.exp(-1j*np.ceil(q/2)*thetas[i-1])
            P[i]/=(result_coll_gffs[0]*2*pi)
        return P

    def residence_probability(self, thetas, e2plot): 
      
        #energies, wfns = self.hb.solve(self.reduced_moment, self.v_coeffs, evecs=True)
        energies = self.energy_levels
        wfns     = self.wfns
        wfns_t = np.transpose(wfns)

        gff  = self.coeffs_to_gff(wfns_t[e2plot])
        mult = self.mult_gff(gff)
        result_coll_gffs = self.int_gff(mult)
        result_coll_Fourier = self.gff_to_coeffs(result_coll_gffs)

        norm = result_coll_Fourier[0]*2*np.pi
      
        P = np.zeros(len(thetas),dtype=float)
        for i in range(len(P)):
            # the constant term becomes linear
            if thetas[i] < thetas[i-1]:# periodic boundary:
                P[i] += result_coll_Fourier[0]*(thetas[i]-0) # const, cos 1, sin 1, cos 2, sin 2, ...
#                print("P["+str(i)+"] = "+str(result_coll_Fourier[0])+"*("+str(thetas[i])+" - 0)="+str(P[i]))
                P[i] += result_coll_Fourier[0]*(2*np.pi-thetas[i-1])
#                print("P["+str(i)+"] +="+str(result_coll_Fourier[0])+"*(2pi-"+str(thetas[i-1])+")="+str(P[i]))
            else:
                P[i] = result_coll_Fourier[0]*(thetas[i]-thetas[i-1]) # const, cos 1, sin 1, cos 2, sin 2, ...
#                print("P["+str(i)+"] = "+str(result_coll_Fourier[0])+"*("+str(thetas[i])+" - "+str(thetas[i-1])+")="+str(P[i]))
            for q in range(1,len(result_coll_Fourier)): #loop through integrated[psi(compl.conj.)*psi] coefficents 
                n = np.ceil(q/2) # 0 -> 0; 1 -> 1; 2 -> 1; 3 -> 2; ...
                if q % 2 != 0: # odd -- cosines
                    P[i] += (result_coll_Fourier[q]*(np.cos(n*thetas[i]) - 
                                                     np.cos(n*thetas[i-1])) )
#                    print("P["+str(i)+"] += "+str(result_coll_Fourier[q])+"*(cos("+str(n*thetas[i])+") - cos("+str(n*thetas[i-1])+")="+str(P[i]))
                else: # even, sines
                    P[i] += (result_coll_Fourier[q]*(np.sin(n*thetas[i]) -  
			                             np.sin(n*thetas[i-1])) )
#                    print("P["+str(i)+"] -= "+str(result_coll_Fourier[q])+"*(sin("+str(n*thetas[i])+") - sin("+str(n*thetas[i-1])+")="+str(P[i]))
            P[i]/=norm
        return P

    def residence_probability_T(self, thetas, T=298.15):
        P = np.zeros(len(thetas),dtype=float)
        Q=0.0
        for i in range(len(self.energy_levels)):
            print("Residence probability at energy level "+str(i))
            P_i = self.residence_probability(thetas,i)
            BoltzmannFactor = np.exp(-self.energy_levels[i]/kjmol/(8.314/1000)/T)
            print("Boltzmann factor for energy "+str(i)+" of "+str(self.energy_levels[i]/kjmol)+" kJ/mol amounts to "+str(BoltzmannFactor))
            Q    += BoltzmannFactor
            for j in range(len(P)):
                P[j] += P_i[j]*BoltzmannFactor
        return np.asarray(P)/Q

    def plot_WF(self, name, e2plot=0):
        #e2plot=4 #energy level for which the corr. wavefunction is plotted

        energies, wfns = self.hb.solve(self.reduced_moment, self.v_coeffs, evecs=True)
        wfns_t = np.transpose(wfns)

        filename = str(name)+'WF'+str(e2plot)+'.png'

        step = 0.001
        x = np.arange(0.0, 2*np.pi*(1+0.5*step), 2*np.pi*step)
        v = self.hb.eval_fn(x, wfns_t[e2plot])+2
        pt.clf()
        self.plot_levels(filename,298.15)
        pt.ylabel('wavefunction')
        pt.xlabel('angle [deg]')
        pt.plot(x/deg, v, "k-", linewidth=2)
        pt.savefig(filename)
        return 0


    #def plot_P(self, name, e2plot1=0,e2plot2=11, e2plot3=12, added_points=[]):
    def plot_P(self, name, e2plot1=0,added_points=[]):
        #e2plot=4 #energy level for which the corr. wavefunction is plotted

#        energies, wfns = self.hb.solve(self.reduced_moment, self.v_coeffs, evecs=True)
        energies = self.energy_levels
        wfns     = self.wfns
        wfns_t = np.transpose(wfns)
        
        filename = str(name)+'P'+str(e2plot1)+'.png'
        step = 0.001
        x = np.arange(0.0, 2*np.pi*(1+0.5*step), 2*np.pi*step)
        gff1 = self.coeffs_to_gff(wfns_t[e2plot1])
        #gff2 = self.coeffs_to_gff(wfns_t[e2plot2])
        #gff3 = self.coeffs_to_gff(wfns_t[e2plot3])
        mult1 = self.mult_gff(gff1)
        #mult2 = self.mult_gff(gff2)
        #mult3 = self.mult_gff(gff3)
        v1 = self.hb.eval_fn(x, self.gff_to_coeffs(mult1)*0.3)+0.22
        #v2 = self.hb.eval_fn(x, self.gff_to_coeffs(mult2))+1.98
        #v3 = self.hb.eval_fn(x, self.gff_to_coeffs(mult3))+2.02
        pt.clf()
        self.plot_levels_all(filename,298.15,added_points=added_points)
        pt.ylabel('Energy (KJ/mol) ')
        pt.xlabel('Dihedral angle [deg]')
        pt.plot(x/deg, v1, "g-", linewidth=2)
        #pt.plot(x/deg, v2, "r-", linewidth=2)
        #pt.plot(x/deg, v3, "m-", linewidth=2)
        pt.savefig(filename,dpi=300)
        return 

      
def calculateArcLength(self, coeffs):
    """Calculates the ArcLength of the function v(grid)
    """
    #for argument 'nmax' in HarmonicBasis the length of 'coeffs' is taken because it sets the limits in the for loop of 'eval_deriv' --> see: rotor.py
    hb2 = HarmonicBasis(int((len(coeffs)-1)/2), 2*np.pi)
    step = 0.001
    x = np.arange(0.0, 2*np.pi*(1+0.5*step), 2*np.pi*step)
    arclength = np.trapz(np.sqrt( (1 + hb2.eval_deriv(x, coeffs)**2)), x)
    return arclength
    

def calculateAverageCurvature(self, coeffs):
    #for argument 'nmax' in HarmonicBasis the length of 'coeffs' is taken because it sets the limits in the for loop of 'eval_deriv2' --> see: rotor.py
    hb2 = HarmonicBasis(int((len(coeffs)-1)/2), 2*np.pi)
    step = 0.001
    x = np.arange(0.0, 2*np.pi*(1+0.5*step), 2*np.pi*step)
    curvature = np.sqrt(np.trapz(np.sqrt( (hb2.eval_deriv2(x, coeffs)**2)), x))
    return curvature


