# Modification module for an optimized fit
#
# This file is NOT part of TAMkin.
#

import tamkin
from tamkin import *
#import rotor

from tamkin.nma import NMA, MBH
from tamkin.geom import transrot_basis

from molmod import deg, kjmol, angstrom, centimeter, amu, boltzmann, \
    lightspeed, cached

import numpy as np

__all__ = [
    "Opt_Rotor"
]

class Opt_Rotor(Rotor):
    """
    insert explanation
    """
    def __init__(self, rot_scan, Molecule=None, cancel_freq='mbh',even=False, rotsym=1, num_levels=100, derivatives=None, weights=None, dofmax=6):
        
        
        self.rot_scan=rot_scan

        # finding out the number of points in rot_scan and saving it in number_rot_scan
        number_rot_scan=len(rot_scan.potential[0])

        self.Molecule=Molecule
        self.cancel_freq=cancel_freq
        self.cancel_freq_arg=cancel_freq
        self.even=even
        self.rotsym=rotsym
        self.num_levels=num_levels
        self.dofmax=dofmax
        
        # the user can choose to hand over specific weights, else the default value will be 1 for each scan point
        if weights==None:
            list_weights =[]
            for i in range(0,number_rot_scan):
                list_weights += [1]
            self.weights=list_weights
        else:
            self.weights=weights
            
        self.derivatives=derivatives

        # initilizing the original Rotor
        Rotor.__init__(self, rot_scan=rot_scan, molecule=Molecule, cancel_freq=cancel_freq, rotsym=rotsym, num_levels=num_levels, dofmax=dofmax, even=False)
        
        

    def init_part_fun(self, nma, partf):
       
        # defining nma_angle
        from molmod.ic import dihed_angle
        self.nma_angle = dihed_angle(nma.coordinates[self.rot_scan.dihedral])[0]
        
        # building the temporary scan 'scantemp'
        angles = [self.nma_angle, self.nma_angle + (np.pi/2), self.nma_angle + np.pi, self.nma_angle - (np.pi/2)]
        energies = [0.0,0.0006,0.001,0.0006]
        scantemp = tamkin.data.RotScan(dihedral=self.rot_scan.dihedral, top_indexes=self.rot_scan.top_indexes, potential=[angles, energies])

        # making a temporary Rotor with scantemp
        RotorTemp=Rotor(scantemp, Molecule, cancel_freq='scan', even=False, num_levels=self.num_levels, dofmax=14)

        # init_part_fun of original rotor.py with scantemp
        RotorTemp.init_part_fun(nma, partf)
        angles, energies = self.potential

        # overwriting v_coeffs with the opt_fit approach
        RotorTemp.v_coeffs=self.opt_fit(angles, energies, self.dofmax, rotsym=RotorTemp.rotsym, even=self.even, v_threshold=self.v_threshold,  num_levels=self.num_levels, weights = self.weights, derivatives = self.derivatives, size= RotorTemp.hb.size)
        RotorTemp.dofmax=self.dofmax

        # solving the Schroedinger equation again for changed potential
        RotorTemp.energy_levels=RotorTemp.hb.solve(RotorTemp.moment, RotorTemp.v_coeffs)
    
        # energy_levels[:num_levels] makes a new list out of energy_levels from index 0 to num_levels
        RotorTemp.energy_levels=RotorTemp.energy_levels[:RotorTemp.num_levels]
        
        # after making the Rotor object 'RotorTemp' with scantemp, the real scan 'rot_scan' is now inserted
        RotorTemp.rot_scan=self.rot_scan
        RotorTemp.potential=self.potential

        # replacing self with RotorTemp
        self.__dict__.update(RotorTemp.__dict__)
        
        # updating several attributes of self
        # the cancelation frequency based on the scan
        if RotorTemp.hb is None:
            if not isinstance(RotorTemp.cancel_freq, float):
                raise ValueError('No cancelation frequency was computed for rotor "%s"' % self.name)
        else:
            self.force_constant = RotorTemp.hb.eval_deriv2(np.array([self.nma_angle]), RotorTemp.v_coeffs)[0]
            self.scan_cancel_freq = np.sqrt(self.force_constant/RotorTemp.moment)/(2*np.pi)
            if self.cancel_freq_arg == 'scan':
                self.cancel_freq = self.scan_cancel_freq
            elif abs(self.cancel_freq - self.scan_cancel_freq) > 0.3*abs(self.cancel_freq):
                print('WARNING: The cancelation frequency of rotor "%s" obtained with MBH (%.1f cm^-1) deviates a lot from the one derived from the scan (%.1f cm^-1).' % (
                    self.name, self.cancel_freq/(lightspeed/centimeter), self.scan_cancel_freq/(lightspeed/centimeter)
                ))
        
        # print a warning is the cancelation frequency is rather high.
        if self.cancel_freq > 500*(lightspeed/centimeter):
            print('WARNING: the cancelation frequency of rotor "%s" is rather high: %.1f cm^-1.' % (
                self.name, self.cancel_freq/(lightspeed/centimeter)
            ))
        elif self.cancel_freq <= 0:
            print('WARNING: the cancelation frequency of rotor "%s" is negative: %.1f cm^-1.' % (
                self.name, self.cancel_freq/(lightspeed/centimeter)
            ))

        # scaling factors
        self.freq_scaling = partf.vibrational.freq_scaling
        self.zp_scaling = partf.vibrational.zp_scaling
        self.classical = partf.vibrational.classical
        

    def opt_fit(self, grid, v, dofmax=50, rotsym=1, even=False, v_threshold=1000.0,  num_levels=100, weights = None, derivatives = None, minimize_curvature=1, rcond=0.0, size= None):
        global curvature_length
        curvature_length = 1000
        """Fit the expansion coefficients that represent function f

           Arguments:
            | ``grid`` -- The x values on which the function f is known.
            | ``v`` -- The function to be represented by expansion coefficients.
            | ``dofmax`` -- The maximum number of cosines in the fit. When
                            even==False, the same number of sines is also
                            included.
            | ``weights`` -- weights is an array with values between 1 and 0 and 
                             the length of the number of scan points. This allows
                             the scan points to be weighted differently for the fit.
            | ``derivatives`` -- 
            | ``minimize_curvature`` -- equals 1 for the optimized fit modification


           Optional arguments:
            | ``rotsym`` -- Impose this rotational symmetry [default=1]. Must be
                            an integer and is least 1..
            | ``even`` -- Only fit even functions, i.e. cosines. [default=False]
            | ``rcond`` -- The cutoff for the singular values in the least
                           squares fit. [default=1e-10]
            | ``v_threshold`` -- Tolerance on the relative error between the
                                 Fourier expansion and the data points of the
                                 scan. [default=0.01]. Absolute errors smaller
                                 than 1 kJ/mol are always ignored.

           In case the Fourier expansion represents a poor fit (determined by
           v_threshold), a ValueError is raised. It means that you have to
           check your torsional scan datapoints for errors.
        """
        #import matplotlib.pyplot as pt
        if rotsym < 1:
            raise ValueError("rotsym must be at least 1.")
          
        # construct the design matrix
        ncos = int(min(dofmax, num_levels/rotsym))
        if derivatives != None:
                [derivative_values, derivative_positions, derivative_weights] = derivatives
        else:
                [derivative_values, derivative_positions, derivative_weights] =[None, None, None]
        if derivative_values == None:
                derivative_length = 0
        else:
            derivative_length = len(derivative_values)
            # construct the design matrix
            ncos = int(min(dofmax, num_levels/rotsym))
        
            minimize_curvature = 1
            # curvature_length = 1000 chosen arbitrarily showing good results
            curvature_length = 1000
        if even:
            A = np.zeros((len(grid)+derivative_length+minimize_curvature*curvature_length, ncos+1), float)
        else:
            A = np.zeros((len(grid)+derivative_length+minimize_curvature*curvature_length, int(2*ncos+1)), float)
        
        # Fill matrix A with values
        A[0:len(grid),0] = weights/np.sqrt(2*np.pi)
        counter = 1
        for i in range(ncos):
            arg = ((i+1)*rotsym*2*np.pi/(2*np.pi))*grid
            A[0:len(grid),counter] = np.cos(arg)/np.sqrt((2*np.pi)/2)*weights
            counter += 1
            if not even:
                A[0:len(grid),counter] = np.sin(arg)/np.sqrt((2*np.pi)/2)*weights
                counter += 1
        #Fill matrix A with derivatives
        #A[0:len(grid),0] = 1.0/np.sqrt((2*np.pi)) - > derivative of constant is 0
        if derivative_length > 0:
                counter = 1
                derivative_positions = np.asarray(derivative_positions)
                for i in range(ncos):
                    factor = rotsym*(2*np.pi)*(i+1)/(2*np.pi)
                    arg = ((i+1)*rotsym*(2*np.pi)/(2*np.pi))*derivative_positions
                    A[(len(grid)):(len(grid)+len(derivative_positions)),counter] = -factor*np.sin(arg)/np.sqrt((2*np.pi)/2)*derivative_weights
                counter += 1
                if not even:
                    A[(len(grid)):(len(grid)+derivative_length),counter] = factor*np.cos(arg)*(i+1)/np.sqrt((2*np.pi)/2)*derivative_weights
                    counter += 1
        
        if minimize_curvature:
                
                curvature_grid = np.linspace(0.0,2*np.pi, curvature_length)
                curvature_grid = np.asarray(curvature_grid)
                counter = 1
                for i in range(ncos):
                        # weight factor at 0.001 chosen arbitrarily showing good results
                        factor = 0.001*(rotsym*2*np.pi*(i+1)/(2*np.pi))**2
                        arg = ((i+1)*rotsym*2*np.pi/(2*np.pi))*curvature_grid
                        A[len(grid)+derivative_length:(len(grid)+derivative_length+curvature_length),counter] = -factor*np.cos(arg)/np.sqrt((2*np.pi)/2)
                        counter +=1
                        if not even:
                                A[(len(grid))+derivative_length:(len(grid)+derivative_length+curvature_length),counter] = -factor*np.sin(arg)/np.sqrt((2*np.pi)/2)
                                counter += 1
                #print(len(A))
        

        v_full = np.zeros(len(v)+derivative_length+minimize_curvature*curvature_length)
        v_full[0:len(v)] = v*weights;
        v_full[len(v):len(v)+derivative_length] = derivative_values
        v_full[len(v)+derivative_length:len(v)+derivative_length+minimize_curvature*curvature_length]  = np.zeros(minimize_curvature*curvature_length, float)
        

        coeffs, residuals, rank, S = np.linalg.lstsq(A, v_full, rcond)

        # check the error
        residual = np.dot(A, coeffs) - v_full
        rmsd = (residual**2).mean()**0.5
        rms = (v_full**2).mean()**0.5
        abs_threshold = max(1*kjmol, rms*v_threshold)
        if (rmsd > abs_threshold).any():
            raise ValueError("Residual is too large (v_th=%f). (poor Fourier expansion.) rmsd [kJ/mol] = %f, rms [kJ/mol] = %f" % (v_threshold, rmsd/kjmol, rms/kjmol))

        # collect the parameters in a convenient array
        result = np.zeros(size)
        result[0] = coeffs[0]

        if even:
            tmp = result[2*rotsym-1::2*rotsym]
            tmp[:ncos] = coeffs[1:]
        else:
            tmp = result[2*rotsym-1::2*rotsym]
            tmp[:ncos] = coeffs[1::2]
            tmp = result[2*rotsym-0::2*rotsym]
            tmp[:ncos] = coeffs[2::2]
        arclength = calculateArcLength(self, coeffs)
        curvature = calculateAverageCurvature(self, coeffs)
        directArclength = 0.0
        for i in range(0, len(grid)-1):
            directArclength += np.sqrt( (v[i]-v[i+1])**2+(grid[i]-grid[i+1])**2)
        return result
      
def calculateArcLength(self, coeffs):
    """Calculates the ArcLength of the function v(grid)
    """
    #for argument 'nmax' in HarmonicBasis the length of 'coeffs' is taken because it sets the limits in the for loop of 'eval_deriv' --> see: rotor.py
    hb2 = HarmonicBasis(int((len(coeffs)-1)/2), 2*np.pi)
    step = 0.001
    x = np.arange(0.0, 2*np.pi*(1+0.5*step), 2*np.pi*step)
    arclength = np.trapz(np.sqrt( (1 + hb2.eval_deriv(x, coeffs)**2)), x)
    return arclength
    

def calculateAverageCurvature(self, coeffs):
    #for argument 'nmax' in HarmonicBasis the length of 'coeffs' is taken because it sets the limits in the for loop of 'eval_deriv2' --> see: rotor.py
    hb2 = HarmonicBasis(int((len(coeffs)-1)/2), 2*np.pi)
    step = 0.001
    x = np.arange(0.0, 2*np.pi*(1+0.5*step), 2*np.pi*step)
    curvature = np.sqrt(np.trapz(np.sqrt( (hb2.eval_deriv2(x, coeffs)**2)), x))
    return curvature
