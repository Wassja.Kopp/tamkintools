from tamkin import *
# NpT is used when nothing else is specified (1bar)
# Reactants:
ho2    = load_molecule_g03fchk("C2_1.fchk")
btfm   = load_molecule_g03fchk("RC2_1.fchk")
#Transition State:
btfmTS = load_molecule_g03fchk("C2_1.fchk")
scanOO = load_rotscan_g03log("C2_12OO.log", top_indexes=[18,19])
scanOH = load_rotscan_g03log("C2_1OHgesamt.log", top_indexes=[19])
#scanOO = load_rotscan_g03log("C2_12OO.log")
#scanOH = load_rotscan_g03log("C2_1OHgesamt.log")
#Products:
h2o2   = load_molecule_g03fchk("h2o2freq.fchk")
rad    = load_molecule_g03fchk("RC2_1.fchk")
 
nma_ho2    = NMA(ho2, ConstrainExt())
nma_btfm   = NMA(btfm, ConstrainExt())
nma_btfmTS = NMA(btfmTS, ConstrainExt())
nma_h2o2   = NMA(h2o2, ConstrainExt())
nma_rad    = NMA(rad, ConstrainExt())
my_weights1 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
my_weights2 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
rotorOO = Rotor(scanOO, btfmTS, rotsym=1, cancel_freq='scan', even=False, dofmax=500, num_levels = 100, weights = my_weights1, derivatives=None)
rotorOH = Rotor(scanOH, btfmTS, rotsym=1, cancel_freq='scan', even=False, dofmax=6, num_levels = 100, weights = my_weights2, derivatives=None)

pfrrho = PartFun(nma_btfmTS, [ExtTrans(), ExtRot(symmetry_number=1)])
pfHR = PartFun(nma_btfmTS, [ExtTrans(), ExtRot(symmetry_number=1), rotorOO, rotorOH])
pfrrho.write_to_file("C2_12_rrho.csv")
pfHR.write_to_file("C2_12_HR.csv")
rotorOO.plot_levels("_RotorOOLevels_C2_12.png",900)
rotorOH.plot_levels("_RotorOHLevels_C2_12.png",900)
 
pf_ho2  = PartFun(nma_ho2, [ExtTrans(), ExtRot(symmetry_number=1)])
pf_btfm = PartFun(nma_btfm, [ExtTrans(), ExtRot(symmetry_number=1)])
 
pf_h2o2  = PartFun(nma_h2o2, [ExtTrans(), ExtRot(symmetry_number=1)])
pf_rad = PartFun(nma_rad, [ExtTrans(), ExtRot(symmetry_number=1)])
 
 
# Thermo Analysis
 
tarrho = ThermoAnalysis(pfrrho, [500, 1000, 1500, 2000])
tahr   = ThermoAnalysis(pfHR,   [500, 1000, 1500, 2000])
tarrho.write_to_file("ThermoHO_2_12.csv")
tahr.write_to_file("ThermoHR_2_12.csv")
 
# Tunneling Corrections:
 
tunneling = Eckart([pf_ho2,pf_btfm],pfHR,[pf_h2o2,pf_rad])
tunneling_rrho = Eckart([pf_ho2,pf_btfm],pfrrho,[pf_h2o2,pf_rad])
 
# b3lyp rate !!!!!!!!!!!1
 
km_HRtun = KineticModel([pf_ho2,pf_btfm],pfHR, tunneling)
km_HRtun.write_to_file("kinetic_2_12.csv")
 
print ("--- Hindered Rotator with tunneling ---")
print ("T/1K     k(T) [%s]" % (km_HRtun.unit_name));
for T in range(300, 2100, 100):
    rc = km_HRtun.rate_constant(T)
    print ("%d   %.5e") % (T, rc/km_HRtun.unit)
 
print ("")
 
km_HR = KineticModel([pf_ho2,pf_btfm],pfHR)
print ("--- Hindered Rotator without tunneling ---")
print ("T/1K     k(T) [%s]") % (km_HR.unit_name);
for T in range(300, 2100, 100):
    rc = km_HR.rate_constant(T)
    print ("%d   %.5e") % (T, rc/km_HR.unit)
 
print ("")
 
print ("--- Harmonic Oscillator with tunneling ---")
 
km_HOtun = KineticModel([pf_ho2,pf_btfm],pfrrho,tunneling_rrho)
 
print ("T/1K     k(T) [%s]") % (km_HOtun.unit_name);
for T in range(300, 2100, 100):
    rc = km_HOtun.rate_constant(T)
    print ("%d   %.5e") % (T, rc/km_HOtun.unit)
print ("")
 
print ("--- Harmonic Oscillator without tunneling ---")
 
km_HO = KineticModel([pf_ho2,pf_btfm],pfrrho)
 
print ("T/1K     k(T) [%s]") % (km_HO.unit_name);
for T in range(300, 2100, 100):
    rc = km_HO.rate_constant(T)
    print ("%d   %.5e") % (T, rc/km_HO.unit)
