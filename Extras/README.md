# TamkinTools

Additional modules for TAMkin

"rotor_modforPY3.py" is the modified version of "rotor.py" in tamkin. For tests put it into python3.6/site-packages/tamkin instead of the old rotor.py and rename "rotor_modforPY3.py" to "rotor.py" for functionality reasons.
